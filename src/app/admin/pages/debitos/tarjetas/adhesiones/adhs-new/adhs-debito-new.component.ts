import { Component, OnInit }                    from '@angular/core';
import { FormBuilder, Validators }              from '@angular/forms';
import { Router }                               from '@angular/router';

import { IonicSelectableComponent }             from 'ionic-selectable';
import { Subscription }                         from 'rxjs';

import { ToastrService }                        from 'ngx-toastr';

import { ApiRestService }                       from '../../../../../../services/api-rest.service';

@Component({
  selector: 'app-adhs-debito-new',
  templateUrl: './adhs-debito-new.component.html',
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'adhs-debito' },
  ]
})
export class AdhsDebitoNewComponent implements OnInit {

  afiliados: any[] = [];
  tarjetas: any[] = [];
  cuentas: any[];

  afiliadosSubscription: Subscription;

  form = this.fb.group({
    afiliado: ['', Validators.required],
    tarjeta: [{value: '', disabled: true}, Validators.required],
    // tipo: ['', Validators.required],
    // importe: ['', Validators.required],
    // fecha: ['', Validators.required],
    cuenta: [{value: '', disabled: true}, Validators.required],
    // meses: ['', Validators.required],
    // estado: ['', Validators.required],
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private fb: FormBuilder,
    private _api: ApiRestService
  ) { }

  ngOnInit() { 
    this._api.getAllFrom('cuentas').subscribe( res =>
      this.cuentas = res
    );
  }

  onBack() {
    this.router.navigate(['admin/debitos-automaticos/adhesiones']);
  }

  // asynchronously
  searchChangeAfiliado(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    // Close any running subscription.
    if (this.afiliadosSubscription) {
      this.afiliadosSubscription.unsubscribe();
    }

    if (!text) {
      // Close any running subscription.
      if (this.afiliadosSubscription) {
        this.afiliadosSubscription.unsubscribe();
      }

      event.component.items = [];
      event.component.endSearch();
      return;
    }

    this.afiliadosSubscription = this._api.getAllFrom('afiliados').subscribe( res => {
      // Subscription will be closed when unsubscribed manually.
      if (this.afiliadosSubscription.closed) {
        return;
      }

      event.component.items = this.filterAfiliados(res, text);
      event.component.endSearch();
    });
  }

  filterAfiliados(afiliados: any[], text: string) {
    return afiliados.filter( el => {
      return el.afi_nombre.toLowerCase().indexOf(text) !== -1 || el.afi_nrodoc.toString().toLowerCase().indexOf(text) !== -1;
    });
  }

  onChangeAfiliado(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    let afiliado = event.value;
    if (afiliado.afi_tarjetas) {
      this.tarjetas = afiliado.afi_tarjetas;
    }
    this.form.get('tarjeta').enable();
  }

  onChangeTarjeta() {
    this.form.get('cuenta').enable();
  }

  onSubmit(form: any){
    
    this._api.add(form).subscribe(
      response => { 
        if (response["status"] == "error") {
          this.toastr.error('Todos los campos deben estar completos!.');
        }else if(response["status"] == "success"){
          this.toastr.success('Cambios guardados');
          this.router.navigate(['admin/debitos-automaticos/adhesiones']).then(() => {
            setTimeout(() => {
              window.location.reload();
            }, 1000);
          });
        }     
      },
      error => {
        console.error(error);
      }
    );

  }
}