// MODULES
import { NgModule }                           from '@angular/core';
import { CommonModule }                       from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { IonicModule }                        from '@ionic/angular';
import { IonicSelectableModule }              from 'ionic-selectable';
import { NgxMaskModule }                      from 'ngx-mask';
import { ToastrModule }                       from 'ngx-toastr';
import { MaterialModule }                     from '../../../thirdmodules/material.module';

import { NotifRoutingModule }                 from './notif-routing.module';

// COMPONENTS
import { NotifComponent }                     from './notif.component';
import { ScrollNotifComponent }               from './scroll-notif/scroll-notif.component';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      
      IonicModule.forRoot(),
      IonicSelectableModule,
      
      NgxMaskModule.forRoot(),
      ToastrModule.forRoot(),
      
      NotifRoutingModule,

      MaterialModule
    ],
    declarations: [      
      NotifComponent,
      
      ScrollNotifComponent
    ],
    providers: [],
  })
  export class NotifModule { }