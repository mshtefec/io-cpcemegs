import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '../../environments/environment';

import { BeneficiosI }      from '../interfaces/beneficios.interface';


@Injectable({
  providedIn: 'root'
})
export class BeneficiosService {

  public url = environment.web_api_url_base + '/api/beneficio';

    constructor(private http: HttpClient) { }

    getBeneficios(): Observable<BeneficiosI[]> {
        return this.http.get<BeneficiosI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }
}





