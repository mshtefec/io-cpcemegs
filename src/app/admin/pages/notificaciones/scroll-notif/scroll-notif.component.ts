import { Component, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

import { ApiRestService }                 from '../../../../services/api-rest.service';

@Component({
  selector: 'app-scroll-notif',
  templateUrl: './scroll-notif.component.html',
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'notificaciones' },
  ]
})
export class ScrollNotifComponent implements OnInit {

  notificaciones: any[];

  constructor(
    public router: Router,
    private _api: ApiRestService,
  ) { }

  ngOnInit() {
    this._api.getAllFrom('historial').subscribe( res =>
      this.notificaciones = res
    );
  }

}
