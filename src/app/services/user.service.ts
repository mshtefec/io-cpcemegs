import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '../../environments/environment';

import { UserI }        from '../interfaces/user.interface'

@Injectable({
  providedIn: 'root'
})
export class UserService { 
    public url = environment.web_api_url_base + '/api/users';

    constructor(private http: HttpClient) { }

    getUsers(): Observable<UserI[]> {
        return this.http.get<UserI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getUser(id: number): Observable<UserI> {
        return this.http.get<UserI>(this.url + '/' + id);
    }
    
    addUser(user: UserI): Observable<UserI> {
        return this.http.post(this.url + '/new', user);
    }

    updateUser(user: UserI): Observable<UserI> {
        return this.http.put(this.url + '/edit/' + user.id, user);
    }
}