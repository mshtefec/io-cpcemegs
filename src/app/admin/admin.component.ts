import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  template: `
  
  <app-sidebar></app-sidebar>
  
  <ion-router-outlet id="main"></ion-router-outlet>
  `,
  styles: [],
})
export class AdminComponent implements OnInit {

  constructor( ) { }

  ngOnInit() { }

}
