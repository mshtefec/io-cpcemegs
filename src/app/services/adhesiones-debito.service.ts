import { Injectable }           from '@angular/core';
import { HttpClient }           from '@angular/common/http';

import { Observable }           from 'rxjs';
import { map }                  from 'rxjs/operators';

import { environment }          from '../../environments/environment';

import { AdhesionesDebitoI }    from '../interfaces/adhesiones-debito.interface';

@Injectable({
  providedIn: 'root'
})
export class AdhesionesDebitoService { 
  public url = environment.web_api_url_base + '/api/adhs-debito';

  constructor(private http: HttpClient) { }

  getAll(): Observable<AdhesionesDebitoI[]> {
    return this.http.get<AdhesionesDebitoI[]>(this.url).pipe( 
      map( data => {
        return data;
      })
    );
  }

  getOne(id: number): Observable<AdhesionesDebitoI> {
    return this.http.get<AdhesionesDebitoI>(this.url + '/' + id);
  }

  add(adh: AdhesionesDebitoI): Observable<AdhesionesDebitoI> {
    return this.http.post(this.url + '/new', adh);
  }

  deleted(id: number): Observable<any> {
    return this.http.delete(this.url + '/deleted/' + id);
  }

}