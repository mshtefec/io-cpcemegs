import { NgModule }                      from '@angular/core';
import { Routes, RouterModule }          from '@angular/router';

import { NotifComponent }                from './notif.component';
import { ScrollNotifComponent }          from './scroll-notif/scroll-notif.component';

const routes: Routes = [
  { 
    path: '', 
    component: NotifComponent,
    children: [
      { path: '',   component: ScrollNotifComponent, },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotifRoutingModule { }
