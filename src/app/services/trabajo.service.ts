import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '../../environments/environment';

import { TrabajoI }       from '../interfaces/trabajo.interface';

@Injectable({
  providedIn: 'root'
})
export class TrabajoService { 
    public url = environment.web_api_url_base + '/api/trabajos';

    constructor(private http: HttpClient) { }

    getTrabajos(): Observable<TrabajoI[]> {
        return this.http.get<TrabajoI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getTrabajo(id: number): Observable<TrabajoI> {
        return this.http.get<TrabajoI>(this.url + '/id/' + id);
    }

    getTrabajoByLegalizacion(id: number): Observable<TrabajoI> {
        return this.http.get<TrabajoI>(this.url + '/legalizacion/' + id);
    }
}