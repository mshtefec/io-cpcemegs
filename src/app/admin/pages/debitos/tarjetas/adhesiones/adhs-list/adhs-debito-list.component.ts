import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

import { AlertController }                from '@ionic/angular';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';

import Swal                               from 'sweetalert2';

import { ApiRestService }                 from '../../../../../../services/api-rest.service';

import { ExporterService }                from '../../../../../../services/exporter.service';

@Component({
  selector: 'app-adhs-debito-list',
  templateUrl: './adhs-debito-list.component.html',
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'adhs-debito' },
  ]
})
export class AdhsDebitoListComponent implements OnInit {

  displayedColumns: string[] = [
    'afi_nrodoc', 
    'afi_nombre',
    'tar_tipo',
    'tar_banco',
    'fecha_adh',
    'cuenta',
    'actions'
  ];

  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public router: Router,
    public alertController: AlertController,
    private _api: ApiRestService,
    private excelService: ExporterService,
  ) { }

  ngOnInit() {
    this._api.getAll().subscribe( res => {

      // const filter: any = res;

      // filter.forEach(m => {
      //   m.afiliado = m.afiliado.afi_nrodoc;
      //   m.tarjeta = m.tarjeta.numero;
      //   m.fecha = m.fecha.date;
      //   m.cuenta = m.cuenta.pla_nropla;
      // });

      this.dataSource.data = res;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['admin/debitos-automaticos/adhesiones/new']);
  }

  onGenerate() {
    this.router.navigate(['admin/debitos-automaticos/recibos/generate']);
  }

  onExport() {
    this.router.navigate(['admin/debitos-automaticos/exportar']);
  }

  onEdit(id: number) {
    
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this._api.deleted(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Elemento borrado.',
              'success'
            )
            this.router.navigate(['admin/debitos-automaticos/adhesiones']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 1000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

}
