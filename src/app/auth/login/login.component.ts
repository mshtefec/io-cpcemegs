import { UserI } from 'src/app/interfaces/user.interface';
import { Component, OnInit }  from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AuthService]
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  // showPassword = false;
  public identity;
  public token: string;

  constructor( 
    public _authService: AuthService,
    public router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  onSubmit(user: UserI) {
    this.spinner.show();
    this.checkRole(user, 'ADMIN', '/admin');
  }

  checkRole(user: UserI, role: string, path: string) {

    this._authService.login(user).subscribe(
      response => {
        if (!response.status || response.status != 'error') {
          this.identity = response;
          //get Token
          this._authService.login(user, true).subscribe(
            response => {
              if (!response.status || response.status != 'error') {
                this.token = response;

                localStorage.setItem('token', this.token);
                localStorage.setItem('identity', JSON.stringify(this.identity));

                this.toastr.success("Accedio correctamente, " + this.identity.username);

                this.router.navigate([path]);

              } else {
                this.toastr.error("Ups, :/ algo no salio bien.");
              }
            },
            error => {
              console.log(error);
            }
          );

        } else {
          this.toastr.error("Usuario o Contraseña incorrectos.");
        }
        this.spinner.hide();
      },
      error => {
        console.log(error);
      }
    );
  }

  goAdmin() {
    this.router.navigate(['/admin']);
  }

  // getInputType() {
  //   if (this.showPassword) {
  //     return 'text';
  //   }
  //   return 'password';
  // }

  // toggleShowPassword() {
  //   this.showPassword = !this.showPassword;
  // }

  ngOnInit() {
    this.logout();
  }

  logout() {
    this.route.params.subscribe(
      params => {
        let out = +params['out'];

        if(out == 1) {
          
          localStorage.removeItem('token');
          localStorage.removeItem('identity');

          this.token = null;
          this.identity = null;

          localStorage.clear();

          this.toastr.warning("Hasta la próxima!!");

          this.router.navigate(['/login']);
        }
      }
    );
  }

}
