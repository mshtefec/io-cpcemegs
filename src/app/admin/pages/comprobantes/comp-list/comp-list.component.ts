import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { ComprobanteI }                   from '../../../../interfaces/comprobante.interface';

import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';
import { ComprobanteService }             from '../../../../services/comprobante.service';

@Component({
  selector: 'app-comp-list',
  templateUrl: './comp-list.component.html',
  styleUrls: ['./comp-list.component.scss']
})
export class CompListComponent implements OnInit {

  displayedColumns: string[] = ['com_nroasi', 'com_nrocom', 'com_proceso', 'com_titulo', 'com_matricula', 'com_total', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private toastr: ToastrService,
    private _comprobanteService: ComprobanteService,
  ) {
    
  }

  ngOnInit() {
    
    this._comprobanteService.getAllByCliAndProcess(4, '02R120').subscribe( res => {
      // const filter: any = res.filter(
      //   c => c.com_proceso == '02R120' && c.com_unegos == 2 && c.com_nrocli == 4
      // );
      this.dataSource.data = res;
    });
     
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['admin/comprobantes/new']);
  }

  onEdit(data: ComprobanteI) {
    this.router.navigate(['admin/comprobantes/edit/' + data.com_nroasi]);
  }

  onShow(id: number) {
    this.router.navigate(['admin/comprobantes/show']);
  }

  onDeleted(id: number) {

    // Swal.fire({

    //   title: 'estas seguro?',
    //   text: 'al eliminar este elemento no se puede revertir!',
    //   icon: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Si, eliminar!'

    // }).then(result => {

    //   if (result.value) {

    //     this.comprobanteService.deletedComprobante(id).subscribe(
    //       response => {
    //         Swal.fire(
    //           'Cambios guardados!',
    //           'Elemento borrado.',
    //           'success'
    //         )
    //         this.router.navigate(['admin/comprobantes']).then(() => {
    //           setTimeout(() => {
    //             window.location.reload();
    //           }, 3000);
    //         });
    //       },
    //       error => {
    //         console.error(error);
    //       }
    //     );

    //   }
    // });
    
  }

}
