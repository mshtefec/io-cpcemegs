import { UserI } from './user.interface';

export interface NotificationI {
    user?: UserI;
    action?: NotifActionType;
    description?: string;
    color?: NotifColorType;
    created_at?: Date;
}