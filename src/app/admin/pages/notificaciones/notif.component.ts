import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notificaciones',
  template: `
  <ion-router-outlet id="notificaciones"></ion-router-outlet>
  `,
  styles: [],
})
export class NotifComponent implements OnInit {

  constructor( ) { }

  ngOnInit() { }

}
