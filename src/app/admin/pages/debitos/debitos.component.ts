import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-debitos',
  template: `
  <ion-router-outlet id="debitos"></ion-router-outlet>
  `,
  styles: [],
})
export class DebitosComponent implements OnInit {

  constructor( ) { }

  ngOnInit() { }

}
