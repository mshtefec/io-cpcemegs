import { NgModule }                      from '@angular/core';
import { Routes, RouterModule }          from '@angular/router';
import { DebitosComponent }              from './debitos.component';

import { AdhsDebitoListComponent }       from './tarjetas/adhesiones/adhs-list/adhs-debito-list.component';
import { AdhsDebitoNewComponent }        from './tarjetas/adhesiones/adhs-new/adhs-debito-new.component';
import { ExportarDebitoComponent }       from './tarjetas/exportar/exportar-debito.component';
import { RecibosDebitoListComponent }    from './tarjetas/recibos/recibos-list/recibos-debito-list.component';
import { GenerarRecibosDebitoComponent } from './tarjetas/recibos/recibos-generate/generar-recibos-debito.component';
import { CListComponent }                from './caja-de-ahorro/c-list/c-list.component';

const routes: Routes = [
  { 
    path: '', 
    component: DebitosComponent,
    children: [
      { path: 'adhesiones',         component: AdhsDebitoListComponent, },
      { path: 'adhesiones/new',     component: AdhsDebitoNewComponent, },
      { path: 'exportar',           component: ExportarDebitoComponent, },
      { path: 'recibos',            component: RecibosDebitoListComponent, },
      { path: 'recibos/generate',   component: GenerarRecibosDebitoComponent, },
      { path: 'cajas-de-ahorro',    component: CListComponent, },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DebitosRoutingModule { }
