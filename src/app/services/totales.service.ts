import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '../../environments/environment';

import { TotalesI }   from '../interfaces/totales.interface';

@Injectable({
  providedIn: 'root'
})
export class TotalesService {

  public url = environment.web_api_url_base + '/api/totales';

  constructor(private http: HttpClient) { }

  getCheques(): Observable<TotalesI[]> {
    return this.http.get<TotalesI[]>(this.url + '/cheques').pipe( 
      map( data => {
          return data;
      })
        
    );
  }
  getTotales(): Observable<TotalesI[]> {
    return this.http.get<TotalesI[]>(this.url).pipe( 
      map( data => {
          return data;
      })
        
    );
  }
}
