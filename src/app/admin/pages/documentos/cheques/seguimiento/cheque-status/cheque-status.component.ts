import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { ChequeEstadoI }                  from '../../../../../../interfaces/cheque-estado.interface';

import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';
import { ChequeService }                  from '../../../../../../services/cheque.service';
import { TrabajoService }                 from '../../../../../../services/trabajo.service';

@Component({
  selector: 'app-cheque-status',
  templateUrl: './cheque-status.component.html',
  styleUrls: ['./cheque-status.component.scss']
})
export class ChequeStatusComponent implements OnInit {

  displayedColumns: string[] = [
    'nro', 
    'importe', 
    'banco', 
    'fec_diferida',
    'fec_comprobante',
    'estado',
    'actions'
  ];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private toastr: ToastrService,
    private _chequeService: ChequeService,
    private _trabajoService: TrabajoService
  ) {
    
  }

  ngOnInit() {
    
    this._chequeService.getCheques().subscribe( res => {
      this.dataSource.data = res;
    });
     
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onShow(id: number) {
    this.router.navigate(['admin/comprobantes/show']);
  }

  onShowTrabajo(id: number) {
    this._trabajoService.getTrabajoByLegalizacion(id).subscribe( res => {
      this.router.navigate(['admin/trabajos/' + res.id]);
    });
  }

  onDeleted(id: number) {

    // Swal.fire({

    //   title: 'estas seguro?',
    //   text: 'al eliminar este elemento no se puede revertir!',
    //   icon: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Si, eliminar!'

    // }).then(result => {

    //   if (result.value) {

    //     this.comprobanteService.deletedComprobante(id).subscribe(
    //       response => {
    //         Swal.fire(
    //           'Cambios guardados!',
    //           'Elemento borrado.',
    //           'success'
    //         )
    //         this.router.navigate(['admin/comprobantes']).then(() => {
    //           setTimeout(() => {
    //             window.location.reload();
    //           }, 3000);
    //         });
    //       },
    //       error => {
    //         console.error(error);
    //       }
    //     );

    //   }
    // });
    
  }

}
