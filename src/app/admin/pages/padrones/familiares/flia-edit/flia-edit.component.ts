import { Component, OnInit }                    from '@angular/core';
import { FormGroup, FormControl, Validators, ReactiveFormsModule, FormArray, FormBuilder }   from '@angular/forms';
import { Router, ActivatedRoute }               from '@angular/router';
import { ToastrService }                        from 'ngx-toastr';
import { FamiliarService }                      from '../../../../../services/familiar.service';
import { FamiliarI }                             from '../../../../../interfaces/familiar.interface';
import { AfiliadoI }                             from '../../../../../interfaces/afiliado.interface';
import { ObraSocial }                             from '../../../../../interfaces/obra-social.interface';
import { stringify } from '@angular/compiler/src/util';
import Swal                               from 'sweetalert2';
import { BeneficiosService }                      from '../../../../../services/beneficios.service';
import { BeneficiosI }                    from '../../../../../interfaces/beneficios.interface'

@Component({
  selector: 'app-flia-edit',
  templateUrl: './flia-edit.component.html',
  styles: [],
})
export class FliaEditComponent implements OnInit {

 
  familiarId: number;
  dateFecnac: string;
  obraSocialArray: ObraSocial[];
  beneficioArray: BeneficiosI[];
  iHaveBeneficiosArray: BeneficiosI[];
  bandera: number;

  afiForm = new FormGroup({
    id: new FormControl('', Validators.required),
    afiliado: new FormControl('', Validators.required),
    nombre: new FormControl('', Validators.required),
    apellido: new FormControl('', Validators.required),
    fecha_nacimiento: new FormControl('', Validators.required),
    direccion: new FormControl('', Validators.required),
    localidad: new FormControl('', Validators.required),
    provincia: new FormControl('', Validators.required),
    codigo_postal: new FormControl('', Validators.required),
    telefono: new FormControl('', Validators.required),
    mail: new FormControl('', Validators.required),
    estado_civil: new FormControl('', Validators.required),
    sexo: new FormControl('', Validators.required),
    cbu: new FormControl('', Validators.required),
    obraSocial: new FormControl('', Validators.required),
    beneficio_fondosolidario: new FormControl(null, Validators.required),
    beneficio_segurodevida: new FormControl(null, Validators.required),
    beneficio_capitalizacion: new FormControl(null, Validators.required),
    cuit: new FormControl(null, Validators.required),
    parentesco: new FormControl(null, Validators.required),
    beneficios: new FormControl('', Validators.required),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private familiarService: FamiliarService,
    private beneficioService: BeneficiosService,
  ) { }
  
  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.familiarId = +params['id'];
        
        setTimeout(() => this.familiarService.getOneFamiliar(this.familiarId).subscribe(
          response => {
            this.afiForm.controls.id.setValue(response.id);
            this.afiForm.controls.afiliado.setValue(response.afiliado);
            this.afiForm.controls.nombre.setValue(response.nombre);
            this.afiForm.controls.apellido.setValue(response.apellido);
            this.afiForm.controls.fecha_nacimiento.setValue(response.fecha_nacimiento.date);
            this.afiForm.controls.direccion.setValue(response.direccion);
            this.afiForm.controls.localidad.setValue(response.localidad);
            this.afiForm.controls.provincia.setValue(response.provincia);
            this.afiForm.controls.codigo_postal.setValue(response.codigo_postal);
            this.afiForm.controls.telefono.setValue(response.telefono);
            this.afiForm.controls.mail.setValue(response.mail);
            this.afiForm.controls.estado_civil.setValue(response.estado_civil);
            this.afiForm.controls.sexo.setValue(response.sexo);
            this.afiForm.controls.cbu.setValue(response.cbu);
            // this.afiForm.controls.obraSocial.setValue(response.obraSocial['id'].toString());
            // this.afiForm.controls.beneficio_fondosolidario.setValue(response.beneficio_fondosolidario);
            // this.afiForm.controls.beneficio_segurodevida.setValue(response.beneficio_segurodevida);
            // this.afiForm.controls.beneficio_capitalizacion.setValue(response.beneficio_capitalizacion);
            this.afiForm.controls.cuit.setValue(response.cuit);
            this.afiForm.controls.parentesco.setValue(response.parentesco);
            // this.iHaveBeneficiosArray = response.beneficios; 
          },
          error => {
            console.error(error);
          }
        ), 1000);
        
      }
    );
         //traigo los beneficios
        //  this.beneficioService.getBeneficios().subscribe(data => {
        //   this.beneficioArray = data.map(e => {
        //     return {
        //       nombre: e.nombre,
        //       id: e.id,
        //       ativo: e.activo,
        //     };
        //   })
        // });
  }
  onSubmit(familiar: FamiliarI){
    familiar.beneficios = this.iHaveBeneficiosArray;
    this.updateRecord(familiar);
  }

  updateRecord(form: FamiliarI) {

    this.familiarService.updateFamiliar(form).subscribe(
      response => { 
        if (response["status"] == "error") {
          this.toastr.error('Todos los campos deben estar completos!.');
        }else if(response["status"] == "success"){
            this.toastr.success('Cambios guardados');
            this.router.navigate(['admin/familiares']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 1000);
            });
        }     
      },
      error => {
        console.error(error);
      }
    );

  }

  agregarBeneficio( event ){
    
  }

  ocultar(){
    document.getElementById('obj1').style.display = 'inline';
    document.getElementById('obj2').style.display = 'none';
  }

  onDeleted(id: BeneficiosI) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {
      if (result.value) {
        var i = this.iHaveBeneficiosArray.indexOf( id );
 
        if ( i !== -1 ) {
            this.iHaveBeneficiosArray.splice( i, 1 );
        }
      }
    });

  }

  onBack() {
    this.router.navigate(['admin/familiares']);
  }

}
