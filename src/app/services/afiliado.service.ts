import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '../../environments/environment';

import { AfiliadoI }      from '../interfaces/afiliado.interface';

@Injectable({
  providedIn: 'root'
})
export class AfiliadoService { 
    public url = environment.web_api_url_base + '/api/afiliados';

    constructor(private http: HttpClient) { }

    getAfiliados(): Observable<AfiliadoI[]> {
        return this.http.get<AfiliadoI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getAfiliado(id: number): Observable<AfiliadoI> {
        return this.http.get<AfiliadoI>(this.url + '/' + id);
    }
    
    addAfiliado(afiliado: AfiliadoI): Observable<AfiliadoI> {
        return this.http.post(this.url + '/new', afiliado);
    }

    updateAfiliado(afiliado: AfiliadoI): Observable<AfiliadoI> {
        return this.http.put(this.url + '/edit/' + afiliado.afi_nrodoc, afiliado);
    }

    searchAfiliadosByDni(search: string): Observable<AfiliadoI[]> {
        return this.http.get<AfiliadoI[]>(this.url + '/search/' + search).pipe( 
            map( data => {
                return data;
            })
        );
    }
}