import { Component, OnInit }                    from '@angular/core';
import { FormBuilder, Validators }              from '@angular/forms';
import { Router, ActivatedRoute }               from '@angular/router';
import { MatTableDataSource }                   from '@angular/material/table';

import { ToastrService }                        from 'ngx-toastr';
import { ApiRestService }                       from './../../../../../services/api-rest.service';
import { NotificationI }                        from 'src/app/interfaces/notification.interface';

@Component({
  selector: 'app-afi-edit',
  templateUrl: './afi-edit.component.html',
  styles: [],
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'afiliados' },
  ]
})
export class AfiEditComponent implements OnInit {

  deliveryDateInfo: String;
  titulosCollection: any[];

  titulosSelecteds: any[] = [];
  displayedColumnsTitulo: string[] = ['titulo', 'matricula', 'actions'];
  dataSourceTitulos = new MatTableDataSource();

  familiaresSelecteds: any[] = [];
  displayedColumnsFamiliar: string[] = ['dni', 'apellido', 'nombre', 'parentesco', 'actions'];
  dataSourceFamiliares = new MatTableDataSource();

  tarjetasSelecteds: any[] = [];
  dataSourceTarjetas = new MatTableDataSource();
  displayedColumnsTarjetas: string[] = ['tipo', 'banco', 'numero', 'nombre', 'fecha_vencimiento', 'actions'];


  afiForm = this.fb.group({
    afi_ficha: [''],
    // datos personales
    afi_zona: ['', Validators.required],
    afi_sexo: ['', Validators.required],
    afi_civil: ['', Validators.required],
    afi_fecnac: ['', Validators.required],
    afi_nombre: ['', Validators.required],
    afi_nrodoc: ['', Validators.required],
    // residencia
    afi_direccion: ['', Validators.required],
    afi_provincia: ['', Validators.required],
    afi_localidad: ['', Validators.required],
    afi_codpos: ['', Validators.required],
    // contacto
    afi_mail: ['',Validators.email],
    afi_telefono2: ['', Validators.required],
    // otros
    afi_titulos: this.fb.array([]),
    afi_tarjetas: this.fb.array([]),
  });

  tituloForm = this.fb.group({
    titulo: ['', Validators.required],
    matricula: ['', Validators.required],
  });

  tarjetaForm = this.fb.group({
    tipo: ['', Validators.required],
    banco: ['', Validators.required],
    numero: ['', Validators.required],
    nombre: ['', Validators.required],
    fecha_vencimiento: ['', Validators.required],
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private _api: ApiRestService
  ) { }
  
  ngOnInit() { 

    this._api.getAllFrom("titulos").subscribe( res =>
      this.titulosCollection = res
    );

    this.route.params.subscribe(
      params => {
        const id = +params['id'];

        this._api.get(id).subscribe(
          response => {
              /* SET PASANDO UN ARREGLO, NO SE PUEDEN OMITIR CLAVES */
              // setTimeout(() => this.afiForm.setValue({
              //   afi_ficha: response.afi_ficha,
              //   afi_zona: response.afi_zona,
              //   afi_nombre: response.afi_nombre,
              //   afi_fecnac: response.afi_fecnac,
              //   afi_nrodoc: response.afi_nrodoc,
              //   afi_cuit: response.afi_cuit,
              //   afi_sexo: response.afi_sexo,
              //   afi_civil: response.afi_civil,
              //   afi_direccion: response.afi_direccion,
              //   afi_localidad: response.afi_localidad,
              //   afi_provincia: response.afi_provincia,
              //   afi_codpos: response.afi_codpos,
              //   afi_mail: response.afi_mail,
              //   afi_mail_alternativo: response.afi_mail_alternativo,
              //   afi_telefono1: response.afi_telefono1,
              //   afi_telefono2: response.afi_telefono2,
              //   afi_titulos: response.afi_titulos,
              // }));
 
              /* SET VALOR POR VALOR */
              // this.afiForm.controls.afi_ficha.setValue(response.afi_ficha);
              // this.afiForm.controls.afi_zona.setValue(response.afi_zona);  
              // this.afiForm.controls.afi_nombre.setValue(response.afi_nombre);
              // this.afiForm.controls.afi_fecnac.setValue(response.afi_fecnac);
              // this.afiForm.controls.afi_nrodoc.setValue(response.afi_nrodoc);
              // this.afiForm.controls.afi_cuit.setValue(response.afi_cuit);  
              // this.afiForm.controls.afi_sexo.setValue(response.afi_sexo);
              // this.afiForm.controls.afi_civil.setValue(response.afi_civil);
              // this.afiForm.controls.afi_direccion.setValue(response.afi_direccion);
              // this.afiForm.controls.afi_localidad.setValue(response.afi_localidad);
              // this.afiForm.controls.afi_provincia.setValue(response.afi_provincia);
              // this.afiForm.controls.afi_codpos.setValue(response.afi_codpos);
              // this.afiForm.controls.afi_mail.setValue(response.afi_mail);
              // this.afiForm.controls.afi_mail_alternativo.setValue(response.afi_mail_alternativo);
              // this.afiForm.controls.afi_telefono1.setValue(response.afi_telefono1);
              // this.afiForm.controls.afi_telefono2.setValue(response.afi_telefono2);
              // this.afiForm.controls.afi_titulos.setValue(response.afi_titulos);
           
              /* SET PASANDO UN ARREGLO, PUEDEN OMITIR CLAVES */
            this.afiForm.patchValue({
              afi_ficha: response.afi_ficha,
              // datos personales
              afi_zona: response.afi_zona,
              afi_sexo: response.afi_sexo,
              afi_civil: response.afi_civil,
              afi_fecnac: response.afi_fecnac.date,
              afi_nombre: response.afi_nombre,
              afi_nrodoc: response.afi_nrodoc,
              // residencia
              afi_direccion: response.afi_direccion,
              afi_provincia: response.afi_provincia,
              afi_localidad: response.afi_localidad,
              afi_codpos: response.afi_codpos,
              // contacto
              afi_mail: response.afi_mail,
              afi_telefono2: response.afi_telefono2,
            });

            this.dataSourceTitulos.data = response.afi_titulos;
            this.titulosSelecteds = response.afi_titulos;

            this.tarjetasSelecteds = response.afi_tarjetas
            const dateMaping: any = this.tarjetasSelecteds;
            dateMaping.forEach(m => {
              m.tipo = m.tipo,
              m.banco = m.banco,
              m.numero = m.numero,
              m.nombre = m.nombre,
              m.fecha_vencimiento = m.fecha_vencimiento.date
            });

            this.dataSourceTarjetas.data = dateMaping;
            
            this.dataSourceFamiliares.data = response.afi_familiares;
          },
        );
      }
    );

  }

  addNewTitulo(tituloForm: any) {
    this.titulosSelecteds = this.dataSourceTitulos.data;
    this.titulosSelecteds.push(tituloForm);
    
    this.dataSourceTitulos.data = this.titulosSelecteds;
    this.dataSourceTitulos._updateChangeSubscription();
    
    this.tituloForm.reset();
  }

  onDeletedTitulo(id: number) {
    
    this.dataSourceTitulos.data.splice(this.dataSourceTitulos.data.indexOf(id), 1);
    this.dataSourceTitulos._updateChangeSubscription();

    this.tituloForm.reset();
  }

  onShowFamiliar(id: number) {
    this.router.navigate(['admin/familiares/' + id]);
  }

  onEditFamiliar(id: number) {
    this.router.navigate(['admin/familiares/edit/' + id]);
  }

  onSubmit(afiliado: any){
    afiliado.afi_titulos = this.titulosSelecteds;
    afiliado.afi_tarjetas = this.tarjetasSelecteds;

    afiliado.afi_tipdoc = "DNI";
    afiliado.afi_tipo = "A";

    let user = JSON.parse(localStorage.getItem('identity'));

    let notif: NotificationI = {
      user: user.username,
      action: NotifActionType.Edit,
      description: "modifico un afiliado",
      color: NotifColorType.Warning,
    };

    this._api.update(afiliado).subscribe(
      response => { 
        if (response["status"] == "error") {
          this.toastr.error(response['message']);
        } else if (response["status"] == "success") {
          this._api.pushNotification(notif).subscribe(
            response => {
                if (response["status"] == "error") {
                  this.toastr.error(response['message']);
                }else if(response["status"] == "success"){
                  this.toastr.success('Se notifico!');
                  this.router.navigate(['admin/afiliados']);
                }      
              }
          );
          this.toastr.success('Cambios guardados');
        }      
      }
    );
  }
  
  addNewTarjeta(tarjetaForm: any) {

    this.tarjetasSelecteds.push(tarjetaForm);

    const dateMaping: any = this.tarjetasSelecteds;
    dateMaping.forEach(m => {
      m.tipo = m.tipo,
      m.banco = m.banco,
      m.numero = m.numero,
      m.nombre = m.nombre,
      m.fecha_vencimiento = m.fecha_vencimiento
    });
    
    this.dataSourceTarjetas.data = dateMaping;
    this.dataSourceTarjetas._updateChangeSubscription();
    
    this.tarjetaForm.reset();
  }

  onDeletedTarjeta(id: number) {
    this.dataSourceTarjetas.data.splice(this.dataSourceTarjetas.data.indexOf(id), 1);
    this.dataSourceTarjetas._updateChangeSubscription();

    this.tarjetaForm.reset();
  }

  onBack() {
    this.router.navigate(['admin/afiliados']);
  }
}