import { NgModule }                                 from '@angular/core';
import { ExtraOptions, PreloadAllModules, RouterModule, Routes }  from '@angular/router';
import { AuthAdminGuard }                           from './guards/auth-admin.guard';

const routes: Routes = [
  // {
  //   path: '',
  //   loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  // },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { 
    path: '',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
  },
  { 
    path: 'admin',
    canActivate: [AuthAdminGuard],
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
  },
];

const config: ExtraOptions = {
    useHash: false,
    relativeLinkResolution: 'legacy'
};

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
