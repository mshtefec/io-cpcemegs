import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth',
  template: `
    <ion-router-outlet></ion-router-outlet>
  `,
  providers: []
})
export class AuthComponent implements OnInit {

  constructor( 
  ) {
  }

  ngOnInit() {
  }

}