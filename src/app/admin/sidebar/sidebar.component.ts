import { Router, RouterEvent }  from '@angular/router';
import { Component, OnInit }    from '@angular/core';
import { MenuController }       from '@ionic/angular';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  providers: []
})
export class SidebarComponent implements OnInit {

  items = [
    {
      title: 'Notificaciones',
      icon: 'alert-circle-outline',
      link: '/admin/notificaciones'
    },
    {
      title: 'Afiliados',
      children: [
        {
          title: 'Profesionales',
          icon: 'person-outline',
          link: '/admin/afiliados'
        },
        {
          title: 'Familiares',
          icon: 'people',
          link: '/admin/familiares'
        },
        {
          title: 'Estudios Contables',
          icon: 'people',
          link: '/admin/estudiosContables'
        },
      ]
    },
    {
      title: 'Débitos Automáticos',
      children: [
        {
          title: 'Adhesiones',
          icon: 'file-tray-stacked-outline',
          link: '/admin/debitos-automaticos/adhesiones'
        },
        {
          title: 'Exportar Banco',
          icon: 'exit-outline',
          link: '/admin/debitos-automaticos/exportar'
        },
        {
          title: 'Generar Recibos',
          icon: 'enter-outline',
          link: '/admin/debitos-automaticos/recibos/generate'
        },
        {
          title: 'Recibos Débitos',
          icon: 'receipt-outline',
          link: '/admin/debitos-automaticos/recibos'
        },
        // {
        //   title: 'Caja de Ahorro',
        //   icon: 'journal',
        //   link: '/admin/debitos-automaticos/cajas-de-ahorro'
        // },
      ]
    },
    // {
    //   title: 'Comprobantes',
    //   icon: 'document',
    //   link: '/admin/comprobantes'
    // },
    // {
    //   title: 'Documentos Contables',
    //   children: [
    //     {
    //       title: 'Cheques Entrantes',
    //       icon: 'cash-outline',
    //       link: '/admin/cheques'
    //     },
    //     {
    //       title: 'Cheques Seguimiento',
    //       icon: 'cash',
    //       link: '/admin/cheques-seguimiento'
    //     },
    //   ]
    // },
    {
      title: 'Salir',
      icon: 'power-outline',
      link: '../logout/1'
    },
  ];

  selectedPath = 'Afiliados';

  constructor( 
    private menuCtrl: MenuController,
    private router: Router
  ) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
  }

  toggle() {
    this.menuCtrl.toggle();
  }

  ngOnInit() { }

}
