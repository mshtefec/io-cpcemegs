import { BeneficiosI } from './../interfaces/beneficios.interface';

export interface FamiliarI {
    id?: number;
    afiliado?: number;
    dni?: number;
    apellido?: string;
    nombre?: string;
    fecha_nacimiento?: any;
    direccion?: string;
    localidad?: string;
    provincia?: string;
    codigo_postal?: number;
    telefono?: number;
    mail?: string;
    estado_civil?: string;
    sexo?: string;
    cbu?: string;
    obraSocial?: string;
    beneficio_fondosolidario?: boolean;
    beneficio_segurodevida?: boolean;
    beneficio_capitalizacion?: boolean;
    beneficios?: BeneficiosI[];
    cuit?: number;
    parentesco?: string;
}
