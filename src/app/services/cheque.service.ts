import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '../../environments/environment';

import { ChequeEstadoI }  from '../interfaces/cheque-estado.interface';

@Injectable({
  providedIn: 'root'
})
export class ChequeService { 
  public url = environment.web_api_url_base + '/api/cheques-seguimiento';

  constructor(private http: HttpClient) { }

  getCheques(): Observable<ChequeEstadoI[]> {
    return this.http.get<ChequeEstadoI[]>(this.url).pipe( 
      map( data => {
        return data;
      })
    );
  }

  updateChequeEstado(cheque: ChequeEstadoI): Observable<ChequeEstadoI> {
    return this.http.put(this.url + '/edit/' + cheque.nro, cheque);
  }
}