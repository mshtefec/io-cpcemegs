import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '../../environments/environment';

import { ComprobanteI }   from '../interfaces/comprobante.interface';

@Injectable({
  providedIn: 'root'
})
export class ComprobanteService { 
    public url = environment.web_api_url_base + '/api/comproba';

    constructor(private http: HttpClient) { }

    getComprobantes(): Observable<ComprobanteI[]> {
        return this.http.get<ComprobanteI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getAllByCliAndProcess(nrocli: number, proceso: string): Observable<ComprobanteI[]> {
        return this.http.get<ComprobanteI[]>(this.url + '-filter' + '/' + nrocli + '/' + proceso).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getComprobante(id: number): Observable<ComprobanteI> {
        return this.http.get<ComprobanteI>(this.url + '/' + id);
    }
    
    addComprobante(comprobante: ComprobanteI): Observable<ComprobanteI> {
        return this.http.post(this.url + '/new', comprobante);
    }

    updateComprobante(comprobante: ComprobanteI): Observable<ComprobanteI> {
        return this.http.put(this.url + '/edit/' + comprobante.com_nroasi, comprobante);
    }
}