
export interface TotalesI {
    totProceso?: string;
    utotNrocom?: number;
    totFecha?: Date;
    totFecven?: Date;
    totForpag?: string;
    totDebe?: number;
    totHabe?: number;
    totSubpla?: string;
    totTipdoc?: string;
    totNrodoc?: number;
    totTitulo?: string;
    totMatricula?: number;
    totNroope?: number;
    totNroreg?: number;
    totCaja?: number;
    totZeta?: number;
    totNrolegali?: number;
    totConcepto?: string;
    totLote?: number;
    totAsiant?: number;
    totEstado?: string;
    totFecconcilia?: Date;
    totOpeconcilia?: number;
    totFecvta?: Date;
    totAsicancel?: number;
    totImppag?: number;
    totCodretencion?: number;
    totBonifica?: number;
    totPorcentaje?: number;
    totSobre?: number;
    totIva?: number;
    totNrochequera?: number;
    totLetcheque?: string;
    totNrocheque?: number;
    totFecche?: Date;
    totFecdif?: Date;
    totTipdes?: string;
    totNrodes?: number;
    totEstche?: string;
    totAsigrupal?: number;
    totFecalt?: Date;
    totNroasi?: number;
    totUnegos?: number;
    totItem?: number;
    totNrocuo?: number;
  
 


   


    
}
