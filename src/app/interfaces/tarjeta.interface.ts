import { AfiliadoI } from './afiliado.interface';

export interface TarjetaI {
    id?: string;
    afiliado?: AfiliadoI;
    numero?: number;
    nombre?: string;
    fecha_vencimiento?: string;
    tipo?: string;
    banco?: string;
    franquicia?: string;
    created_at?: Date;
    updated_at?: Date;
}