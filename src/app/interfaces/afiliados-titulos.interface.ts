import { AfiliadoI } from './afiliado.interface';
import { TituloI } from './titulo.interface';

export interface AfiliadosTitulosI {
    afiliado?: AfiliadoI;
    titulo?: TituloI;
    matricula?: number;
}