import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';

import { ApiRestService }                 from '../../../../../../services/api-rest.service';

@Component({
  selector: 'app-recibos-debito-list',
  templateUrl: './recibos-debito-list.component.html',
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'recibos-debito' },
  ]
})
export class RecibosDebitoListComponent implements OnInit {

  displayedColumns: string[] = [
    'afi_nrodoc', 
    'afi_nombre',
    'importe',
    'fecha_adh',
    'cuenta',
    'actions'
  ];

  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public router: Router,
    private _api: ApiRestService
  ) { }

  ngOnInit() {
    
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
