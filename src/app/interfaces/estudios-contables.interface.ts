import { AfiliadoI } from './afiliado.interface';

export interface EstudiosContablesI {
    id?: number;
    denominacion?: string;
    cuit?: number;
    zona?: string;
    direccion?: string;
    localidad?: string;
    provincia?: string;
    codigo_postal?: number;
    email?: string;
    telefono?: string;
    integrantes?: AfiliadoI[];
}
