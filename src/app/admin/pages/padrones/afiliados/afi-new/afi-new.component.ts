import { Component, OnInit }                    from '@angular/core';
import { FormBuilder, Validators }              from '@angular/forms';
import { Router }                               from '@angular/router';
import { MatTableDataSource }                   from '@angular/material/table';

import { ToastrService }                        from 'ngx-toastr';
import { ApiRestService }                       from './../../../../../services/api-rest.service';

@Component({
  selector: 'app-afi-new',
  templateUrl: './afi-new.component.html',
  styles: [],
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'afiliados' },
  ]
})
export class AfiNewComponent implements OnInit {

  deliveryDateInfo: String;

  afiForm = this.fb.group({
    afi_ficha: [''],
    // datos personales
    afi_zona: ['', Validators.required],
    afi_sexo: ['', Validators.required],
    afi_civil: ['', Validators.required],
    afi_fecnac: ['', Validators.required],
    afi_nombre: ['', Validators.required],
    afi_nrodoc: ['', Validators.required],
    // residencia
    afi_direccion: ['', Validators.required],
    afi_localidad: ['', Validators.required],
    afi_provincia: ['', Validators.required],
    afi_codpos: ['', Validators.required],
    // contacto
    afi_telefono2: ['', Validators.required], // celular
    afi_mail: ['', Validators.required],
    // otros datos
    afi_titulos: this.fb.array([]),
    afi_tarjetas: this.fb.array([]),
  });

  // valores por defecto
  zonaSelectedDefault = 'SC';
  sexoSelectedDefault = 'M';
  civilSelectedDefault = 'S';

  tituloForm = this.fb.group({
    titulo: ['', Validators.required],
    matricula: ['', Validators.required],
  });

  titulosCollection: any[]; // esto es para el select de titulos

  titulosSelecteds: any[] = [];
  displayedColumnsTitulo: string[] = ['titulo', 'matricula', 'actions'];
  dataSourceTitulos = new MatTableDataSource();

  tarjetaForm = this.fb.group({
    tipo: ['', Validators.required],
    banco: ['', Validators.required],
    numero: ['', Validators.required],
    nombre: ['', Validators.required],
    fecha_vencimiento: ['', Validators.required],
  });

  tarjetasSelecteds: any[] = [];
  dataSourceTarjetas = new MatTableDataSource();
  displayedColumnsTarjetas: string[] = ['tipo', 'banco', 'numero', 'nombre', 'fecha_vencimiento', 'actions'];

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private fb: FormBuilder,
    private _api: ApiRestService
  ) { }

  ngOnInit() { 
    this._api.getAllFrom("titulos").subscribe( res =>
      this.titulosCollection = res
    );
  }

  addNewTitulo(tituloForm: any) {
    
    this.titulosSelecteds.push(tituloForm);
    
    this.dataSourceTitulos.data = this.titulosSelecteds;
    this.dataSourceTitulos._updateChangeSubscription();

    this.tituloForm.reset();
  }

  onDeletedTitulo(id: number) {
    
    this.dataSourceTitulos.data.splice(this.dataSourceTitulos.data.indexOf(id), 1);
    this.dataSourceTitulos._updateChangeSubscription();

    this.tituloForm.reset();
  }

  addNewTarjeta(tarjetaForm: any) {
    
    this.tarjetasSelecteds.push(tarjetaForm);
    this.dataSourceTarjetas.data = this.tarjetasSelecteds;
    this.dataSourceTarjetas._updateChangeSubscription();
    this.tarjetaForm.reset();
  }

  onDeletedTarjeta(id: number) {
    
    this.dataSourceTarjetas.data.splice(this.dataSourceTarjetas.data.indexOf(id), 1);
    this.dataSourceTarjetas._updateChangeSubscription();

    this.tarjetaForm.reset();
  }

  onSubmit(afiliado: any) {

    afiliado.afi_titulos = this.titulosSelecteds;
    afiliado.afi_tarjetas = this.tarjetasSelecteds;
    
    this._api.add(afiliado).subscribe(
      response => {
          if (response["status"] == "error") {
            this.toastr.error(response['message']);
          }else if(response["status"] == "success"){
            this.toastr.success('Cambios guardados');
            this.router.navigate(['admin/afiliados']);
          }      
        }
    );

  }

  onBack() {
    this.router.navigate(['admin/afiliados']);
  }
}