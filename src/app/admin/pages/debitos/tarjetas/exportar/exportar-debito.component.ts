import { Component, OnInit }                    from '@angular/core';
import { FormBuilder, Validators }              from '@angular/forms';
import { Router }                               from '@angular/router';
import { DatePipe }                             from '@angular/common';

import { MatTableDataSource }                   from '@angular/material/table';

import { ToastrService }                        from 'ngx-toastr';
import { ExporterService }                      from 'src/app/services/exporter.service';
import { ApiRestService }                       from 'src/app/services/api-rest.service';

@Component({
  selector: 'app-exportar-debito',
  templateUrl: './exportar-debito.component.html',
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'exportar-debito' },
  ]
})
export class ExportarDebitoComponent implements OnInit {

  showDetails: boolean = false;
  isAdhs: boolean = false;

  json: string[] = [];

  displayedColumnsAdheridos: string[] = [
    'afi_nrodoc', 
    'afi_nombre',
    'tar_tipo',
    'tar_banco',
    'fecha_adh',
    'cuenta'
  ];

  dataSourceAdheridos = new MatTableDataSource();

  tarjetas_tipo: any[] = [
    { nombre: "Tuya" },
    { nombre: "Visa" },
    { nombre: "Mastercard" }
  ];

  cuentas: any[];
  convenios: any[];
  convenio_tipo: any;
  adhesiones: any[];
  adhesiones_tipo: any[];

  form = this.fb.group({
    cuenta: ['', Validators.required],
    tarjeta_tipo: [{value: '', disabled: true}, Validators.required],
    fecha_generacion: [{value: '', disabled: true}, Validators.required],
  });

  formShow = this.fb.group({
    comercio: [''],
    tarjeta: [''],
    cuenta: [''],
    registros: [''],
    total: [''],
  });

  constructor(
    public router: Router,
    private fb: FormBuilder,
    private _api: ApiRestService,
    private _expService: ExporterService,
    private toastr: ToastrService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() { 
    this._api.getAllFrom('cuentas').subscribe( res =>
      this.cuentas = res
    );
  }

  onChangeCuenta() {
    this._api.getAllFrom('adhs-debito').subscribe( res => {
      // traigo todo y filtro adhesiones por la cuenta seleccionada.
      this.adhesiones = res.filter(
        adh => adh.cuenta.numero == this.form.value.cuenta.numero
      );

      this._api.getAllFrom('convenios').subscribe( res => {
        this.convenios = res.filter(
          conv => conv.cuenta.numero == this.form.value.cuenta.numero
        );
        if (this.convenios.length != 0) {
          this.toastr.info('Existen #' + this.convenios.length + ' convenio/s con esta cuenta.');
        } else {
          this.toastr.warning('No existen convenios con esta cuenta!!.');
        }
      });

      if (this.adhesiones.length != 0) {
        this.toastr.info('Existen #' + this.adhesiones.length + ' afiliado/s adherido/s al débito automático de esta cuenta.');
      } else {
        this.toastr.warning('No existen afiliados adheridos al débito automático de esta cuenta!!.');
      }

    });
    
    this.form.get('tarjeta_tipo').enable();
  }

  onChangeTipo() {
    // ahora filtro por tarjeta seleccionada.
    this.adhesiones_tipo = this.adhesiones.filter(
      adh => adh.tarjeta.tipo == this.form.value.tarjeta_tipo.nombre
    );

    // vuelvo a filtrar los convenios para obtener el deseado.
    let convenio = this.convenios.filter(
      conv => conv.tarjeta_tipo == this.form.value.tarjeta_tipo.nombre
    );
    if (convenio.length != 0) {
      this.convenio_tipo = convenio[0];
      this.toastr.info('Convenio encontrado para esta cuenta y tipo de tarjeta.');
    } else {
      this.toastr.warning('No se encontro convenio para esta cuenta y tipo de tarjeta!!.');
    }

    this.dataSourceAdheridos.data = this.adhesiones_tipo;

    if (this.adhesiones_tipo.length != 0) {
      this.isAdhs = true;
      this.toastr.info('Existen #' + this.adhesiones_tipo.length + ' afiliado/s adherido/s al débito automático de esta cuenta con tarjetas tipo ' + this.form.value.tarjeta_tipo.nombre + '.');
    } else {
      this.isAdhs = false;
      this.toastr.warning('No existen afiliados adheridos al débito automático de esta cuenta con este tipo de tarjetas!!.');
    }

    // this.form.get('cuenta').disable(); // no se como bloquearlo pero que quede con su valor.
    this.form.get('fecha_generacion').enable();
  }

  onSubmit(form: any){

    /* HEADER */
    /* NRO COMERCIO - TIPO DE REGISTRO "1" - FECHA DE PRESENTACIÓN (ddmmyy) - DENOMINACIÓN - TIPO DEBITO "000" - 40 ESPACIOS EN BLANCO */
    // 3110300165205 1 010321 00000000000013040100  000
    /* DATA */
    /* 
      NRO COMERCIO (13 números) - 
      TIPO DE REGISTRO "2" - 
      NRO DE TARJETA (16 números) - 
      REFERENCIA (12 números cuit en este caso del afiliado) - 
      CUOTA ACTIVA (3 números) - 
      CUOTAS CANTIDAD DEL PLAN (3 números) - 
      FRECUENCIA DB "00" - 
      IMPORTE P. ENTERA (9 números) - 
      IMPORTE P. DECIMAL (2 números) - 
      PERIODO (5 alf.) - 
      TIPO MONEDA (1) PESO (2) DOLAR - 
      COD DE ERROR (2 VACIO) - USO INTERNO (16 VACIO) 
    */
    // 3110300165205 2 4509953566233704 020365636460 001 001 00 000001250 50 ENE01 1                  
    // 3110300165205 2 4509953566233704 020365636460 001 002 00 000001250 50 FEB01 1
    /* FOOTER */
    /* NRO COMERCIO - TIPO DE REGISTRO "9" - FECHA DE PRESENTACIÓN (ddmmyy) - CANTIDAD DE REGISTROS () - IMPORTE P. ENTERA (9 números) - IMPORTE P. DECIMAL (2 números) - TIPO DEBITO "000" - 40 ESPACIOS EN BLANCO */
    // 3110300165205 9 010321 0000002 0000000002501 00 000
    if (this.convenio_tipo) {
      
      let importe_total: number = 0;
      
      let header: string = this.convenio_tipo.nro_comercio + "1" + this.datePipe.transform(form.fecha_generacion, 'ddMMyy') + form.cuenta.numero.padStart(20, "0") + "  " + "000" + "".padStart(40, " ") + "\n";
      this.json.push(header);

      this.adhesiones.forEach(adh => {
        let importe_mensual_convenio = Number(this.convenio_tipo.importe_mensual).toFixed(2).split(".");
        // de momento voy a tomar el importe mensual del convenio pero es necesario que traiga el importe directamente desde el proceso y la categoria correspondiente del afiliado.
        let dato: string = this.convenio_tipo.nro_comercio + "2" + adh.tarjeta.numero + adh.afiliado.afi_nrodoc.toString().padStart(12, "0") + "001" + "001" + "00" + importe_mensual_convenio[0].padStart(9, "0") + importe_mensual_convenio[1].padStart(2, "0") + this.datePipe.transform(form.fecha_generacion, 'MMM').replace(".", "").toUpperCase() + this.datePipe.transform(form.fecha_generacion, 'dd') + "1" + "".padStart(2, " ") + "".padStart(16, " ") + "\n";
        // sumo importe total.
        importe_total += Number(this.convenio_tipo.importe_mensual);
        this.json.push(dato);
      });

      // separo en un arreglo la parte entera de la parte decimal, importante toFixed(2) me separa la parte decimal teniendo en cuenta 2 decimales. por ejemplo si no usara toFixed(2) el ej: 2050.50 => devuelve 5 en vez de 50.
      let importe: string[] = importe_total.toFixed(2).split(".");
      
      let footer: string = this.convenio_tipo.nro_comercio + "9" + this.datePipe.transform(form.fecha_generacion, 'ddMMyy') + this.adhesiones.length.toString().padStart(7, "0") + importe[0].padStart(13, "0") + importe[1] + "000" + "".padStart(40, " ");
      this.json.push(footer);
      
      this.showDetails = true;

      let archivo: any[] = [
        { 
          convenio: this.convenio_tipo,
          cantidad_adhesiones: this.adhesiones_tipo.length,
          importe_total: importe_total.toFixed(2)
        }
      ];

      this.formShow.controls.comercio.patchValue(this.convenio_tipo.nro_comercio);
      this.formShow.controls.tarjeta.patchValue(this.convenio_tipo.tarjeta_tipo);
      this.formShow.controls.cuenta.patchValue(this.convenio_tipo.cuenta.descripcion);
      this.formShow.controls.registros.patchValue(this.adhesiones_tipo.length);
      this.formShow.controls.total.patchValue(importe_total.toFixed(2));      
    } else {
      this.toastr.error('No se puede generar el archivo porque no existen convenios para esta cuenta o este tipo de tarjeta.');
    }

  }

  onExport() {
    let filename: string = "C" + this.convenio_tipo.nro_comercio.substr(-7) + "." + this.datePipe.transform(this.form.value.fecha_generacion, 'dd') + this.datePipe.transform(this.form.value.fecha_generacion, 'MMMMM');
    this._expService.exportToCsv(this.json, filename);
  }

  onBack() {
    this.router.navigate(['admin/debitos-automaticos/adhesiones']);
  }

  onReset() {
    this.form.reset();
    this.formShow.reset();
    this.form.get('tarjeta_tipo').disable();
    this.form.get('fecha_generacion').disable();
    this.json = [];
    this.convenios = [];
    this.convenio_tipo = null;
    this.showDetails = false;
    this.isAdhs = false;
  }

}

