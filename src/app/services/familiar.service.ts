import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '../../environments/environment';

import { FamiliarI }      from '../interfaces/familiar.interface';

@Injectable({
  providedIn: 'root'
})
export class FamiliarService {

  public url = environment.web_api_url_base + '/api/familiares';

    constructor(private http: HttpClient) { }

    getFamiliares(): Observable<FamiliarI[]> {
        return this.http.get<FamiliarI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }

    getOneFamiliar(id: number): Observable<FamiliarI> {
        return this.http.get<FamiliarI>(this.url + '/' + id);
    }
    
    addFamiliar(familiar: FamiliarI): Observable<FamiliarI> {
        return this.http.post(this.url + '/new', familiar);
    }

    updateFamiliar(familiar: FamiliarI): Observable<FamiliarI> {
        return this.http.put(this.url + '/edit/' + familiar.id, familiar);
    }

    deletedFamiliar(id: number): Observable<FamiliarI> {
        return this.http.delete(this.url + '/deleted/' + id);
    }
}
