import { IonicModule }              from '@ionic/angular';
import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import { ReactiveFormsModule }      from '@angular/forms';
import { AuthRoutingModule }        from '../auth/auth-routing.module';
import { AuthComponent }            from '../auth/auth.component';
import { LoginComponent }           from '../auth/login/login.component';


import { FormsModule }              from '@angular/forms';
import { HttpClientModule }         from '@angular/common/http';
import { NgxSpinnerModule }         from 'ngx-spinner';
import { ToastrModule }             from 'ngx-toastr';

import { AuthService }              from '../services/auth.service';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    AuthRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ToastrModule.forRoot(),
    
    NgxSpinnerModule

  ],
  declarations: [
    AuthComponent,
    LoginComponent
  ],
  providers: [
    AuthService
  ],
})
export class AuthModule { }
