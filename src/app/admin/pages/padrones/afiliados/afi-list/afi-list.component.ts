import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { ApiRestService }                 from './../../../../../services/api-rest.service';
import { AlertController }                from '@ionic/angular';

@Component({
  selector: 'app-afi-list',
  templateUrl: './afi-list.component.html',
  styles: [],
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'afiliados' },
  ]
})
export class AfiListComponent implements OnInit {

  displayedColumns: string[] = [
    'afi_nrodoc', 
    //'titulo', 
    //'matricula', 
    'afi_nombre', 
    'afi_mail', 
    'actions'
  ];

  dataSource = new MatTableDataSource();
  //dataSource = new MatTableDataSource<AfiliadoI>();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    public alertController: AlertController,
    private _api: ApiRestService
  ) {
    
  }

  ngOnInit() {
    this._api.getAll().subscribe( res => {
      this.dataSource.data = res;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onShow(id: number) {
    this.router.navigate(['admin/afiliados/' + id]);
  }

  onNew() {
    this.router.navigate(['admin/afiliados/new']);
  }

  onEdit(id: number) {
    this.router.navigate(['admin/afiliados/edit/' + id]);
  }

  async showTuto() {
    const alert = await this.alertController.create({
      cssClass: 'instructions-alert',
      header: 'Instructivo',
      subHeader: 'Filtros',
      message: '<img src="assets/instructivos/cpce_inst_filtros.gif"/>',
      buttons: ['Salir']
    });

    await alert.present();
  }

}
