import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '../../environments/environment';

import { TituloI }        from '../interfaces/titulo.interface';


@Injectable({
  providedIn: 'root'
})
export class TituloService {
  public url = environment.web_api_url_base + '/api/titulos';

    constructor(private http: HttpClient) { }

    getAll(): Observable<TituloI[]> {
      return this.http.get<TituloI[]>(this.url).pipe( 
          map( data => {
              return data;
          })
      );
    }
}
