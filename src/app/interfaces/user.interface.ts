export interface UserI {
    id?: string;
    username?: string;
    email?: string;
    password?: string;
    get_token?: boolean;
}