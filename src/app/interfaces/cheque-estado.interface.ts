export interface ChequeEstadoI {
    nro?: number;
    asiento?: number;
    legalizacion?: number;
    importe?: number;
    banco?: string;
    fec_diferida?: Date;
    fec_comprobante?: Date;
    estado?: string;
}