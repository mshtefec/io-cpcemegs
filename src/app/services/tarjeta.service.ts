import { Injectable }     from '@angular/core';
import { HttpClient }     from '@angular/common/http';

import { Observable }     from 'rxjs';
import { map }            from 'rxjs/operators';

import { environment }    from '../../environments/environment';

import { TarjetaI }      from '../interfaces/tarjeta.interface';

@Injectable({
  providedIn: 'root'
})
export class TarjetaService { 
    public url = environment.web_api_url_base + '/api/tarjetas';

    constructor(private http: HttpClient) { }

    getTarjetas(): Observable<TarjetaI[]> {
        return this.http.get<TarjetaI[]>(this.url).pipe( 
            map( data => {
                return data;
            })
        );
    }
}