import { FamiliarI } from './familiar.interface';
import { AfiliadosTitulosI } from './afiliados-titulos.interface';

export interface AfiliadoI {
    afi_tipdoc?: string;
    afi_nrodoc?: number;
    afi_cuit?: string;
    afi_fecnac?: Date;
    afi_nombre?: string;
    afi_sexo?: string;
    afi_civil?: string;
    afi_direccion?: string;
    afi_localidad?: string;
    afi_provincia?: string;
    afi_codpos?: number;
    afi_telefono1?: string;
    afi_telefono2?: string;
    afi_mail?: string;
    afi_mail_alternativo?: string;
    afi_categoria?: string;
    afi_ficha?: string;
    obraSocial?: number;
    afi_tipo?: string;
    afi_matricula?: number;
    afi_zona?: string;
    afi_familiares?: FamiliarI[];
    afi_titulos?: AfiliadosTitulosI[];
}