import { NgModule }                      from '@angular/core';
import { Routes, RouterModule }          from '@angular/router';
import { AdminComponent }                from './admin.component';
import { AfiListComponent }              from './pages/padrones/afiliados/afi-list/afi-list.component';
import { AfiNewComponent }               from './pages/padrones/afiliados/afi-new/afi-new.component';
import { AfiEditComponent }              from './pages/padrones/afiliados/afi-edit/afi-edit.component';
import { AfiShowComponent }              from './pages/padrones/afiliados/afi-show/afi-show.component';
import { CompListComponent }             from './pages/comprobantes/comp-list/comp-list.component';
import { TrabShowComponent }             from './pages/documentos/trabajos/trab-show/trab-show.component';
import { ChequeListComponent }           from './pages/documentos/cheques/cheque-list/cheque-list.component';
import { ChequeStatusComponent }         from './pages/documentos/cheques/seguimiento/cheque-status/cheque-status.component';
import { EstudiosContablesListComponent} from './pages/padrones/estudiosContables/estudios-contables-list/estudios-contables-list.component';
import { EstudiosContablesNewComponent}  from './pages/padrones/estudiosContables/estudios-contables-new/estudios-contables-new.component';
import { EstudiosContablesShowComponent} from './pages/padrones/estudiosContables/estudios-contables-show/estudios-contables-show.component';
import { EstudiosContablesEditComponent} from './pages/padrones/estudiosContables/estudios-contables-edit/estudios-contables-edit.component';
import { FliaEditComponent }             from './pages/padrones/familiares/flia-edit/flia-edit.component';
import { FliaListComponent}              from './pages/padrones/familiares/flia-list/flia-list.component';
import { FliaNewComponent}               from './pages/padrones/familiares/flia-new/flia-new.component';
import { FliaShowComponent}              from './pages/padrones/familiares/flia-show/flia-show.component';

const routes: Routes = [
  { 
    path: '', 
    component: AdminComponent,
    children: [
      { 
        path: 'notificaciones',
        loadChildren: () => import('./pages/notificaciones/notif.module').then(m => m.NotifModule),
      },
      { path: 'afiliados',                        component: AfiListComponent, },
      { path: 'afiliados/new',                    component: AfiNewComponent, },
      { path: 'afiliados/edit/:id',               component: AfiEditComponent, },
      { path: 'afiliados/:id',                    component: AfiShowComponent, },
      { 
        path: 'debitos-automaticos',
        loadChildren: () => import('./pages/debitos/debitos.module').then(m => m.DebitosModule),
      },
      { path: 'comprobantes',                     component: CompListComponent, },
      { path: 'trabajos/:id',                     component: TrabShowComponent, },
      { path: 'cheques',                          component: ChequeListComponent, },
      { path: 'cheques-seguimiento',              component: ChequeStatusComponent, },
      
      { path: 'estudiosContables',                component: EstudiosContablesListComponent, },
      { path: 'estudiosContables/new',            component: EstudiosContablesNewComponent, },
      { path: 'estudiosContables/edit/:id',       component: EstudiosContablesEditComponent, },
      { path: 'estudiosContables/:id',            component: EstudiosContablesShowComponent, },
      
      { path: 'familiares',                       component: FliaListComponent, },
      { path: 'familiares/new',                   component: FliaNewComponent, },
      { path: 'familiares/edit/:id',              component: FliaEditComponent, },
      { path: 'familiares/:id',                   component: FliaShowComponent, },
      // { 
      //   path: 'debitos-automaticos',
      //   children: [
      //     { path: 'tarjetas',           component: TarjetaListComponent, },
      //   ]
      // },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
