import { Component, OnInit }                    from '@angular/core';
import { Validators, FormBuilder }              from '@angular/forms';
import { Router, ActivatedRoute }               from '@angular/router';

import { MatTableDataSource }                   from '@angular/material/table';

import { ApiRestService }                       from './../../../../../services/api-rest.service';

@Component({
  selector: 'app-afi-show',
  templateUrl: './afi-show.component.html',
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'afiliados' },
  ]
})
export class AfiShowComponent implements OnInit {

  dataSourceTit = new MatTableDataSource();
  displayedTitulo: string[] = [
    'titulo',
    'matricula'
  ];

  dataSourceFam = new MatTableDataSource();
  displayedFamiliar: string[] = [
    'apellido',
    'nombre',
    'dni',
    'parentesco'
  ];
  
  dataSourceTar = new MatTableDataSource();
  displayedTarjetas: string[] = [
    'banco', 
    'tipo', 
    'fecha_vencimiento'
  ];
  
  dateFecnac: string;

  afiForm = this.fb.group({
    afi_nrodoc: ['', Validators.required],
    afi_tipdoc: ['', Validators.required],
    afi_fecnac: ['', Validators.required],
    afi_nombre: ['', Validators.required],
    afi_sexo: [false, Validators.required],
    afi_civil: ['', Validators.required],
    afi_direccion: ['', Validators.required],
    afi_localidad: ['', Validators.required],
    afi_provincia: ['', Validators.required],
    afi_codpos: ['', Validators.required],
    afi_telefono2: ['', Validators.required],
    afi_mail: ['', Validators.required],
    afi_categoria: ['', Validators.required],
    afi_ficha: ['-', Validators.required],
    afi_obrasocial: ['', Validators.required],
    afi_tipo: ['', Validators.required],
    afi_matricula: ['', Validators.required],
    afi_zona: ['', Validators.required],
    afi_familiares: ['', Validators.required],
  });

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private _api: ApiRestService
  ) { }
  
  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        const id = +params['id'];
        this._api.get(id).subscribe(
          response => {

            this.afiForm.patchValue({
              afi_ficha: response.afi_ficha,
              afi_zona: response.afi_zona,
              afi_nombre: response.afi_nombre,
              afi_fecnac: response.afi_fecnac,
              afi_nrodoc: response.afi_nrodoc,
              afi_sexo: response.afi_sexo,
              afi_civil: response.afi_civil,
              afi_direccion: response.afi_direccion,
              afi_localidad: response.afi_localidad,
              afi_provincia: response.afi_provincia,
              afi_codpos: response.afi_codpos,
              afi_mail: response.afi_mail,
              afi_telefono2: response.afi_telefono2,
            });
            
            this.dataSourceTit.data = response.afi_titulos;
            this.dataSourceFam.data = response.afi_familiares;
            this.dataSourceTar.data = response.afi_tarjetas;

          },
        );
      }
    );
  }

  onBack() {
    this.router.navigate(['admin/afiliados']);
  }


  
}