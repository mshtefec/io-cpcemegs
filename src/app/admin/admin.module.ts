// MODULES
import { NgModule }                           from '@angular/core';
import { CommonModule, DatePipe }             from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { IonicModule }                        from '@ionic/angular';
import { IonicSelectableModule }              from 'ionic-selectable';
import { ToastrModule }                       from 'ngx-toastr';
import { NgxMaskModule }                      from 'ngx-mask';
import { AdminRoutingModule }                 from './admin-routing.module';
import { MaterialModule }                     from '../thirdmodules/material.module';

// SERVICES
import { AfiliadoService }                    from '../services/afiliado.service';
import { ComprobanteService }                 from '../services/comprobante.service';
import { ChequeService }                      from '../services/cheque.service';
import { EstudioContableService }             from '../services/estudio-contable.service';
import { TituloService }                      from '../services/titulo.service';

// COMPONENTS
import { AdminComponent }                     from './admin.component';
import { SidebarComponent }                   from './sidebar/sidebar.component'
import { AfiListComponent }                   from './pages/padrones/afiliados/afi-list/afi-list.component';
import { AfiNewComponent }                    from './pages/padrones/afiliados/afi-new/afi-new.component';
import { AfiEditComponent }                   from './pages/padrones/afiliados/afi-edit/afi-edit.component';
import { AfiShowComponent }                   from './pages/padrones/afiliados/afi-show/afi-show.component';
import { CompListComponent }                  from './pages/comprobantes/comp-list/comp-list.component';
import { TrabShowComponent }                  from './pages/documentos/trabajos/trab-show/trab-show.component';
import { ChequeListComponent }                from './pages/documentos/cheques/cheque-list/cheque-list.component';
import { ChequeStatusComponent }              from './pages/documentos/cheques/seguimiento/cheque-status/cheque-status.component';
import { EstudiosContablesEditComponent }     from './pages/padrones/estudiosContables/estudios-contables-edit/estudios-contables-edit.component';
import { EstudiosContablesListComponent }     from './pages/padrones/estudiosContables/estudios-contables-list/estudios-contables-list.component';
import { EstudiosContablesNewComponent }      from './pages/padrones/estudiosContables/estudios-contables-new/estudios-contables-new.component';
import { EstudiosContablesShowComponent }     from './pages/padrones/estudiosContables/estudios-contables-show/estudios-contables-show.component';
import { FliaEditComponent }                  from './pages/padrones/familiares/flia-edit/flia-edit.component';
import { FliaListComponent}                   from './pages/padrones/familiares/flia-list/flia-list.component';
import { FliaNewComponent}                    from './pages/padrones/familiares/flia-new/flia-new.component';
import { FliaShowComponent}                   from './pages/padrones/familiares/flia-show/flia-show.component';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      
      IonicModule.forRoot(),
      IonicSelectableModule,

      NgxMaskModule.forRoot(),
      
      ToastrModule.forRoot(),
      
      AdminRoutingModule,

      MaterialModule,
    ],
    declarations: [      
      AdminComponent,

      SidebarComponent,
      //componentes
      AfiListComponent,
      AfiNewComponent,
      AfiEditComponent,
      AfiShowComponent,
      CompListComponent,
      TrabShowComponent,
      ChequeListComponent,
      ChequeStatusComponent,
      EstudiosContablesEditComponent,
      EstudiosContablesListComponent,
      EstudiosContablesShowComponent,
      EstudiosContablesNewComponent,
      FliaEditComponent,
      FliaListComponent,
      FliaShowComponent,
      FliaNewComponent
    ],
    providers: [
      AfiliadoService,
      ComprobanteService,
      ChequeService,
      EstudioContableService,
      TituloService,
      DatePipe
    ],
  })
  export class AdminModule { }