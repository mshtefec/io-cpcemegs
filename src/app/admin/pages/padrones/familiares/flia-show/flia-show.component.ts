import { Component, OnInit }                    from '@angular/core';
import { FormGroup, FormControl, Validators }   from '@angular/forms';
import { Router, ActivatedRoute }               from '@angular/router';
import { ToastrService }                        from 'ngx-toastr';
import { FamiliarService }                      from '../../../../../services/familiar.service';
import { FamiliarI }                             from '../../../../../interfaces/familiar.interface';
import { BeneficiosService }                      from '../../../../../services/beneficios.service';
import { BeneficiosI }                    from '../../../../../interfaces/beneficios.interface'

@Component({
  selector: 'app-flia-show',
  templateUrl: './flia-show.component.html',
  styles: [],
})
export class FliaShowComponent implements OnInit {

  familiarId: number;
  dateFecnac: string;
  beneficioArray: BeneficiosI[];
  iHaveBeneficiosArray: BeneficiosI[];

  afiForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    apellido: new FormControl('', Validators.required),
    fecha_nacimiento: new FormControl('', Validators.required),
    direccion: new FormControl('', Validators.required),
    localidad: new FormControl('', Validators.required),
    provincia: new FormControl('', Validators.required),
    codigo_postal: new FormControl('', Validators.required),
    telefono: new FormControl('', Validators.required),
    mail: new FormControl('', Validators.required),
    estado_civil: new FormControl('', Validators.required),
    sexo: new FormControl('', Validators.required),
    cbu: new FormControl('', Validators.required),
    obra_social: new FormControl('', Validators.required),
    beneficio_fondosolidario: new FormControl(null, Validators.required),
    beneficio_segurodevida: new FormControl(null, Validators.required),
    beneficio_capitalizacion: new FormControl(null, Validators.required),
    beneficios: new FormControl('', Validators.required),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private familiarService: FamiliarService,
    private beneficioService: BeneficiosService,
  ) { }
  
  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.familiarId = +params['id'];
        this.familiarService.getOneFamiliar(this.familiarId).subscribe(
          response => {
            this.afiForm.controls.nombre.setValue(response.nombre);
            this.afiForm.controls.apellido.setValue(response.apellido);
            this.afiForm.controls.fecha_nacimiento.setValue(response.fecha_nacimiento.date);
            this.afiForm.controls.direccion.setValue(response.direccion);
            this.afiForm.controls.localidad.setValue(response.localidad);
            this.afiForm.controls.provincia.setValue(response.provincia);
            this.afiForm.controls.codigo_postal.setValue(response.codigo_postal);
            this.afiForm.controls.telefono.setValue(response.telefono);
            this.afiForm.controls.mail.setValue(response.mail);
            this.afiForm.controls.estado_civil.setValue(response.estado_civil);
            this.afiForm.controls.sexo.setValue(response.sexo);
            this.afiForm.controls.cbu.setValue(response.cbu);
            // this.afiForm.controls.obra_social.setValue(response.obraSocial['nombre'].toString());
            // this.afiForm.controls.beneficio_fondosolidario.setValue(response.beneficio_fondosolidario);
            // this.afiForm.controls.beneficio_segurodevida.setValue(response.beneficio_segurodevida);
            // this.afiForm.controls.beneficio_capitalizacion.setValue(response.beneficio_capitalizacion);
            // this.iHaveBeneficiosArray = response.beneficios; 
          },
          error => {
            console.error(error);
          }
        );
      }
    );
      //traigo los beneficios
      // this.beneficioService.getBeneficios().subscribe(data => {
      //   this.beneficioArray = data.map(e => {
      //     return {
      //       nombre: e.nombre,
      //       id: e.id,
      //       ativo: e.activo,
      //     };
      //   })
      // });
  }

  onBack() {
    this.router.navigate(['admin/familiares']);
  }

}
