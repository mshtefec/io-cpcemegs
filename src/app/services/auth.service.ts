import { Injectable }                from '@angular/core';
import { HttpClient, HttpHeaders }   from '@angular/common/http';
import { Observable }                from 'rxjs';
import { environment }               from '../../environments/environment';
import { UserI }                     from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public url: string;
  public identity;
  public token;

  user$: Observable<any>;
  
  constructor(
    private _http: HttpClient
  ) { 
    this.url = environment.web_api_url_base;
  }

  login(user: UserI, get_token = null): Observable<any> {

    if (get_token != null) {
      user.get_token = true;
    }
    
    let json = JSON.stringify(user);
    
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencode'
      })
    };

    return this._http.post(this.url + '/login', json, httpOptions);
  }

  getIdentity() {
    let identity = JSON.parse(localStorage.getItem('identity'));

    if (identity && identity != 'undefined') {
      this.identity = identity;
    } else {
      this.identity = null;
    }

    return this.identity;
  }

  getToken() {
    let token = JSON.parse(localStorage.getItem('token'));

    if (token && token != 'undefined') {
      this.token = token;
    } else {
      this.token = null;
    }

    return this.token;
  }

}
