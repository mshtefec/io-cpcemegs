const enum NotifColorType {
    Primary = 'primary',
    Warning = 'warning',
    Danger = 'danger',
    Success = 'success'
}