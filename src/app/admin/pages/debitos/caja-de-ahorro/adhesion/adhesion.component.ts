import { Component, OnInit }                    from '@angular/core';
import { FormGroup, FormControl, Validators }   from '@angular/forms';
import { Router, ActivatedRoute }               from '@angular/router';
import { ToastrService }                        from 'ngx-toastr';

import { AdhesionesDebitoService }              from './../../../../../services/adhesiones-debito.service';

import { AdhesionesDebitoI }                    from 'src/app/interfaces/adhesiones-debito.interface';

@Component({
  selector: 'app-adhesion',
  templateUrl: './adhesion.component.html',
  styleUrls: ['./adhesion.component.scss']
})
export class AdhesionComponent implements OnInit {

  displayedColumns: string[] = [
    'afi_id',
    'tipo',
    'fecha', 
    'cuenta',
    'concepto',
    'estado',
    'actions'
  ];
  
  adhesionForm = new FormGroup({
    afi_id: new FormControl('', Validators.required),
    tipo: new FormControl('', Validators.required),
    fecha: new FormControl('', Validators.required),
    cuenta: new FormControl('', Validators.required),
    concepto: new FormControl('', Validators.required),
    estado: new FormControl('', Validators.required)
  });

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private _adhsDebito: AdhesionesDebitoService
  ) { }

  ngOnInit() { }

  onSubmit(adh: AdhesionesDebitoI) {
    this.route.params.subscribe(
      params => {
        this._adhsDebito.add(adh).subscribe(
          response => {
            if (response["status"] == "error") {
              this.toastr.error(response['message']);
            } else if (response["status"] == "success") {
              this.toastr.success('Adhesión Agregada');
              this.router.navigate(['admin/debitos-automaticos/cajas-de-ahorro']);
            }

          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onBack() {
    this.router.navigate(['admin/debitos-automaticos/cajas-de-ahorro']);
  }
}