import { Component, OnInit }                    from '@angular/core';
import { FormGroup, FormControl, Validators }   from '@angular/forms';
import { Router, ActivatedRoute }               from '@angular/router';
import { ToastrService }                        from 'ngx-toastr';
import { EstudioContableService }               from '../../../../../services/estudio-contable.service';
import { EstudiosContablesI }                   from '../../../../../interfaces/estudios-contables.interface';

@Component({
  selector: 'app-estudios-contables-show',
  templateUrl: './estudios-contables-show.component.html',
  styleUrls: ['./estudios-contables-show.component.scss'],
})
export class EstudiosContablesShowComponent implements OnInit {

  estudioContableId: number;
  deliveryDateInfo: String;
  integrantes = new Array();

  afiForm = new FormGroup({
    denominacion: new FormControl('', Validators.required),
    cuit: new FormControl('', Validators.required),
    zona: new FormControl('', Validators.required),
    direccion: new FormControl('', Validators.required),
    localidad: new FormControl('', Validators.required),
    provincia: new FormControl('', Validators.required),
    codigo_postal: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    telefono: new FormControl('', Validators.required),
    integrantes: new FormControl('', Validators.required),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private estudioContableService: EstudioContableService,
  ) { }
  
  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.estudioContableId = +params['id'];
        this.estudioContableService.getOneEstudioContable(this.estudioContableId).subscribe(
          response => {
            this.afiForm.controls.denominacion.setValue(response.denominacion);
            this.afiForm.controls.cuit.setValue(response.cuit);
            this.afiForm.controls.zona.setValue(response.zona);
            this.afiForm.controls.direccion.setValue(response.direccion);
            this.afiForm.controls.localidad.setValue(response.localidad);
            this.afiForm.controls.provincia.setValue(response.provincia);
            this.afiForm.controls.codigo_postal.setValue(response.codigo_postal);
            this.afiForm.controls.email.setValue(response.email);  
            this.afiForm.controls.telefono.setValue(response.telefono); 
            this.integrantes = response.integrantes;
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onBack() {
    this.router.navigate(['admin/estudiosContables']);
  }

}
