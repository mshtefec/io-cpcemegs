export interface ComprobanteI {
    com_unegos?: number;
    com_nrocli?: number;
    com_nroasi?: number;
    com_nrocom?: number;
    com_proceso?: string;
    com_titulo?: string;
    com_matricula?: number;
    com_importe?: number;
}