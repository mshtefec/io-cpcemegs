export interface AdhesionesDebitoI {
    id?: number;
    afi_id?: number;
    tipo?: string;
    cbu_id?: number;
    tarjeta_id?: number;
    importe?: number;
    fecha?: Date;
    cuenta?: number;
    concepto?: string;
    meses?: number;
    estado?: string;
}