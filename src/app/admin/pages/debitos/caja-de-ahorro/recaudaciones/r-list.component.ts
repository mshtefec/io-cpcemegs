import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

import { AlertController }                from '@ionic/angular';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

// import { AfiliadoI }                      from '../../../../../interfaces/afiliado.interface';
// import { TarjetaI }                       from '../../../../../interfaces/tarjeta.interface';

// import Swal                               from 'sweetalert2';
// import { ToastrService }                  from 'ngx-toastr';
// import { TarjetaService }                 from '../../../../../services/tarjeta.service';

@Component({
  selector: 'app-r-list',
  templateUrl: './r-list.component.html',
  styleUrls: ['./r-list.component.scss']
})
export class RListComponent implements OnInit {

  displayedColumns: string[] = [
    'numero',
    'banco',
    'mes', 
    'actions'
  ];

  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    // private toastr: ToastrService,
    public alertController: AlertController,
    // private _tarjetaService: TarjetaService,
  ) {
    
  }

  ngOnInit() {
    
    // this._tarjetaService.getTarjetas().subscribe( res =>
    //   this.dataSource.data = res
    // );
     
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    // this.router.navigate(['admin/afiliados/new']);
  }

  onEdit(data: any) {
    // this.router.navigate(['admin/afiliados/edit/' + data.afi_nrodoc]);
  }

  onShow(id: number) {
    // this.router.navigate(['admin/afiliados/show']);
  }

  async onDeleted(id: number) {

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'estas seguro?',
      message: 'al eliminar este elemento no se puede revertir!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si, eliminar!',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();

    // Swal.fire({

    //   title: 'estas seguro?',
    //   text: 'al eliminar este elemento no se puede revertir!',
    //   icon: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Si, eliminar!'

    // }).then(result => {

    //   if (result.value) {

    //     this.afiliadoService.deletedAfiliado(id).subscribe(
    //       response => {
    //         Swal.fire(
    //           'Cambios guardados!',
    //           'Elemento borrado.',
    //           'success'
    //         )
    //         this.router.navigate(['admin/afiliados']).then(() => {
    //           setTimeout(() => {
    //             window.location.reload();
    //           }, 3000);
    //         });
    //       },
    //       error => {
    //         console.error(error);
    //       }
    //     );

    //   }
    // });
    
  }

}
