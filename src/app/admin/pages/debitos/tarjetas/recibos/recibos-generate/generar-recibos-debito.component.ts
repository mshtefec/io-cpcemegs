import { Component, OnInit }                    from '@angular/core';
import { FormBuilder, Validators }              from '@angular/forms';
import { Router }                               from '@angular/router';
import { ApiRestService }                       from '../../../../../../services/api-rest.service';
import { ToastrService }                        from 'ngx-toastr';

@Component({
  selector: 'app-generar-recibos-debito',
  templateUrl: './generar-recibos-debito.component.html',
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'recibos-debito' },
  ]
})
export class GenerarRecibosDebitoComponent implements OnInit {

  form = this.fb.group({
    cuenta: ['', Validators.required],
    fecha_generacion: ['', Validators.required],
  });

  cuentas = [
    { name: 'Deudores Derecho Ejer. Profesional', code: '13010200' },
    { name: 'Deudores Derecho Asociados', code: '13010600' },
    { name: 'Deudores Aportes', code: '13040100' },
    { name: 'Deudores Afiliados Ayuda Personal', code: '13051000' },
    { name: 'Deudores Asociados Ayuda Personal', code: '13051100' },
    { name: 'Deudores Moratoria Aportes', code: '13040400' },
  ];
  
  constructor(
    public router: Router,
    private fb: FormBuilder,
    private _api: ApiRestService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {}

  onSubmit(form: any){
    this._api.getAllFrom("adhs-debito").subscribe( res => {
    console.log(res);
    //para poder filtrar todos los adheridos a la cuenta seleccionada tengo que crear otro metodos.
    //osea actualmente la api solo me perdite traer todos los adheridos.
    });
  }

  onBack() {
    this.router.navigate(['admin/debitos-automaticos/adhesiones']);
  }

}
