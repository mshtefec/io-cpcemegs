import { Component, OnInit }                    from '@angular/core';
import { FormGroup, FormControl, Validators, ReactiveFormsModule, FormArray, FormBuilder }   from '@angular/forms';
import { Router, ActivatedRoute }               from '@angular/router';
import { ToastrService }                        from 'ngx-toastr';
import { EstudioContableService }               from '../../../../../services/estudio-contable.service';
import { EstudiosContablesI }                   from '../../../../../interfaces/estudios-contables.interface';
import { AfiliadoService }                      from '../../../../../services/afiliado.service';
import { AfiliadoI}                             from '../../../../../interfaces/afiliado.interface';
import Swal                               from 'sweetalert2';
@Component({
  selector: 'app-estudios-contables-edit',
  templateUrl: './estudios-contables-edit.component.html',
  styleUrls: ['./estudios-contables-edit.component.scss'],
})
export class EstudiosContablesEditComponent implements OnInit {

  estudioContableId: number;
  deliveryDateInfo: String;
  EcArray: AfiliadoI[];
  selectedEcArray = new Array();
  textoBuscar: '';

  afiForm = new FormGroup({
    id: new FormControl('', Validators.required),
    denominacion: new FormControl('', Validators.required),
    cuit: new FormControl('', Validators.required),
    zona: new FormControl('', Validators.required),
    direccion: new FormControl('', Validators.required),
    localidad: new FormControl('', Validators.required),
    provincia: new FormControl('', Validators.required),
    codigo_postal: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    telefono: new FormControl('', Validators.required),
    integrantes: new FormControl('', Validators.required),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private ECService: EstudioContableService,
    private _ECService: AfiliadoService,
    // private datePipe: DatePipe,
    private fb: FormBuilder
  ) { }
  
  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.estudioContableId = +params['id'];
        this.ECService.getOneEstudioContable(this.estudioContableId).subscribe(
          response => {
            console.log(response.id);
            this.afiForm.controls.id.setValue(response.id);
            this.afiForm.controls.denominacion.setValue(response.denominacion);
            this.afiForm.controls.cuit.setValue(response.cuit);  
            this.afiForm.controls.zona.setValue(response.zona); 
            this.afiForm.controls.direccion.setValue(response.direccion);
            this.afiForm.controls.localidad.setValue(response.localidad);
            this.afiForm.controls.provincia.setValue(response.provincia);
            this.afiForm.controls.codigo_postal.setValue(response.codigo_postal);
            this.afiForm.controls.email.setValue(response.email);
            this.afiForm.controls.telefono.setValue(response.telefono);
            this.afiForm.controls.integrantes.setValue(response.integrantes);
            this.selectedEcArray = response.integrantes;
          },
          error => {
            console.error(error);
          }
        );
      }
      );
      //traigo los contadores para cargarlos dentro de integrantes
      this._ECService.getAfiliados().subscribe(data => {
  
      this.EcArray = data.map(e => {
        return {
          afi_nombre: e.afi_nombre,
          afi_matricula: e.afi_matricula,
          afi_nrodoc: e.afi_nrodoc
        };
      })
    });

  }
  onSubmit(afiliado: EstudiosContablesI){

    afiliado.integrantes = this.selectedEcArray;

    this.route.params.subscribe(
      params => {
        // afiliado.integrantes = this.profileForm.value['aliases'][0];
        this.ECService.updateEstudioContable(afiliado).subscribe(
          response => { 
            if (response["status"] == "error") {
              this.toastr.error(response['message']);
            }else if(response["status"] == "success"){
              this.toastr.success('Cambios guardados');
              this.router.navigate(['admin/estudiosContables']).then(() => {
                setTimeout(() => {
                  window.location.reload();
                  }, 100);
                }
              );
            }         
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  ocultar(){
    document.getElementById('obj1').style.display = 'inline';
    document.getElementById('obj2').style.display = 'none';
  }

  onBack() {
    this.router.navigate(['admin/estudiosContables']);
  }

  profileForm = this.fb.group({
    aliases: this.fb.array([
      this.fb.control('')
    ])
  });

  get aliases() {
    return this.profileForm.get('aliases') as FormArray;
  }

  addAlias() {
    this.aliases.push(this.fb.control(''));
  }
  
  buscar( event ){
    this.textoBuscar = event.detail.value
    document.getElementById('obj1').style.display = 'none';
    document.getElementById('obj2').style.display = 'inline';
  }
  addIntegrante(a: EstudiosContablesI){
    this.selectedEcArray.push(a);
    this.toastr.success("Integrante Agregado");
    this.textoBuscar = "";
  }

  onDeleted(id: AfiliadoI) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {
      if (result.value) {
        var i = this.selectedEcArray.indexOf( id );
 
        if ( i !== -1 ) {
            this.selectedEcArray.splice( i, 1 );
        }
      }
    });
  }
}
