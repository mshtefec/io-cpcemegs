import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { EstudiosContablesI }                      from '../../../../../interfaces/estudios-contables.interface';

import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';
import { EstudioContableService }                from '../../../../../services/estudio-contable.service';

@Component({
  selector: 'app-estudios-contables-list',
  templateUrl: './estudios-contables-list.component.html',
  styleUrls: ['./estudios-contables-list.component.scss'],
})
export class EstudiosContablesListComponent implements OnInit {

  displayedColumns: string[] = [
    'denominacion',
    'cuit', 
    'zona', 
    'email', 
    'direccion', 
    'localidad', 
    'provincia',
    'actions'
  ];

  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private toastr: ToastrService,
    private afiliadoService: EstudioContableService,
  ) {
    
  }

  ngOnInit() {
    
    this.afiliadoService.getEstudioContable().subscribe( res =>
      this.dataSource.data = res
    );
     
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['admin/estudiosContables/new']);
  }

  onEdit(data: EstudiosContablesI) {
    this.router.navigate(['admin/estudiosContables/edit/' + data.id]);
  }

  onShow(data: EstudiosContablesI) {
    this.router.navigate(['admin/estudiosContables/show/' + data.id]);
  }

}
