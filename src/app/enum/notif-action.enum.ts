const enum NotifActionType {
    New = 'Creación',
    Edit = 'Modificación',
    Deleted = 'Borrado'
}