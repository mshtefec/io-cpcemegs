import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';
import Swal                               from 'sweetalert2';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

//modelos
import { FamiliarI }                      from '../../../../../interfaces/familiar.interface';

//servicios
import { ToastrService }                  from 'ngx-toastr';
import { FamiliarService }                from '../../../../../services/familiar.service';
import { ApiRestService }                 from './../../../../../services/api-rest.service';

@Component({
  selector: 'app-flia-list',
  templateUrl: './flia-list.component.html',
  styles: [],
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'familiares' },
  ]
})
export class FliaListComponent implements OnInit {

  displayedColumns: string[] = ['afi_dni', 'dni', 'nombre', 'parentesco', 'actions'];

  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private toastr: ToastrService,
    private familiarService: FamiliarService,
    private _api: ApiRestService,
  ) {
    
  }

  ngOnInit() {
    this._api.getAll().subscribe( res =>
      this.dataSource.data = res
    );
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew() {
    this.router.navigate(['admin/familiares/new']);
  }

  onEdit(data: FamiliarI) {
    this.router.navigate(['admin/familiares/edit/' + data.dni]);
  }

  onShow(data: FamiliarI) {
    this.router.navigate(['admin/familiares/' + data.dni]);
  }

  onDeleted(id: number) {

    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {

        this.familiarService.deletedFamiliar(id).subscribe(
          response => {
            Swal.fire(
              'Cambios guardados!',
              'Elemento borrado.',
              'success'
            )
            this.router.navigate(['admin/familiares']).then(() => {
              setTimeout(() => {
                window.location.reload();
              }, 1000);
            });
          },
          error => {
            console.error(error);
          }
        );

      }
    });
    
  }

}
