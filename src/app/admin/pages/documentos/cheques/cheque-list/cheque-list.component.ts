import { Component, ViewChild, OnInit }   from '@angular/core';
import { Router }                         from '@angular/router';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { TotalesI }                       from '../../../../../interfaces/totales.interface';
import { ChequeEstadoI }                  from './../../../../../interfaces/cheque-estado.interface';

import Swal                               from 'sweetalert2';
import { ToastrService }                  from 'ngx-toastr';
import { TotalesService }                 from '../../../../../services/totales.service';
import { TrabajoService }                 from './../../../../../services/trabajo.service';
import { ChequeService }                  from './../../../../../services/cheque.service';

@Component({
  selector: 'app-cheque-list',
  templateUrl: './cheque-list.component.html',
  styleUrls: ['./cheque-list.component.scss']
})
export class ChequeListComponent implements OnInit {

  displayedColumns: string[] = [
    'tot_nrocheque', 
    'tot_nroasi', 
    'tot_nrolegali', 
    'tot_debe', 
    'tot_concepto', 
    'tot_fecdif', 
    'tot_fecalt', 
    'actions',
    'status'
  ];

  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    public router: Router,
    private toastr: ToastrService,
    private _totalesService: TotalesService,
    private _trabajoService: TrabajoService,
    private _chequeService: ChequeService
  ) {
    
  }

  ngOnInit() {
    
    this._totalesService.getCheques().subscribe( res => {
      this.dataSource.data = res;
    });
     
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // onNew() {
  //   this.router.navigate(['admin/comprobantes/new']);
  // }

  // onEdit(data: ComprobanteI) {
  //   this.router.navigate(['admin/comprobantes/edit/' + data.com_nroasi]);
  // }

  onShow(id: number) {
    this.router.navigate(['admin/comprobantes/show']);
  }

  onShowTrabajo(id: number) {
    this._trabajoService.getTrabajoByLegalizacion(id).subscribe( res => {
      this.router.navigate(['admin/trabajos/' + res.id]);
    });
  }

  onUpdateEstado(cheque: any, estado: string) {
    
    

    this._chequeService.updateChequeEstado(null).subscribe(
      response => { 
        if (response["status"] == "error") {
          this.toastr.error(response['message']);
        }else if(response["status"] == "success"){

          this.toastr.success('Cambios guardados');
          this.router.navigate(['admin/cheques-seguimiento']);
        }    
         
      },
      error => {
        console.error(error);
      }
    );
  }

  onDeleted(id: number) {

    // Swal.fire({

    //   title: 'estas seguro?',
    //   text: 'al eliminar este elemento no se puede revertir!',
    //   icon: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Si, eliminar!'

    // }).then(result => {

    //   if (result.value) {

    //     this.comprobanteService.deletedComprobante(id).subscribe(
    //       response => {
    //         Swal.fire(
    //           'Cambios guardados!',
    //           'Elemento borrado.',
    //           'success'
    //         )
    //         this.router.navigate(['admin/comprobantes']).then(() => {
    //           setTimeout(() => {
    //             window.location.reload();
    //           }, 3000);
    //         });
    //       },
    //       error => {
    //         console.error(error);
    //       }
    //     );

    //   }
    // });
    
  }

}
