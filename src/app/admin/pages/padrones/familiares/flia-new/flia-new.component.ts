import { Component, OnInit }                    from '@angular/core';
import { FormGroup, FormControl, Validators }   from '@angular/forms';
import { Router }                               from '@angular/router';

import { IonicSelectableComponent }             from 'ionic-selectable';

import { ToastrService }                        from 'ngx-toastr';
import { ApiRestService }                       from './../../../../../services/api-rest.service';

import { FamiliarI }                            from '../../../../../interfaces/familiar.interface';
import { AfiliadoI }                            from '../../../../../interfaces/afiliado.interface';

@Component({
  selector: 'app-flia-new',
  templateUrl: './flia-new.component.html',
  styles: [],
  providers: [ ApiRestService,
    { provide: 'path', useValue: 'familiares' },
  ]
})
export class FliaNewComponent implements OnInit {

  afiliados: AfiliadoI[];
  
  familiarForm = new FormGroup({
    afiliado: new FormControl('', Validators.required),
    parentesco: new FormControl('', Validators.required),
    // datos personales
    sexo: new FormControl('', Validators.required),
    estado_civil: new FormControl('', Validators.required),
    fecha_nacimiento: new FormControl('', Validators.required),
    nombre: new FormControl('', Validators.required),
    apellido: new FormControl('', Validators.required),
    dni: new FormControl(null, Validators.required),
    // residencia
    direccion: new FormControl('', Validators.required),
    provincia: new FormControl('', Validators.required),
    localidad: new FormControl('', Validators.required),
    codigo_postal: new FormControl('', Validators.required),
    // contacto
    mail: new FormControl('', Validators.required),
    telefono: new FormControl('', Validators.required),
  });

  // valores por defecto
  parentescoSelectedDefault = 'E';
  sexoSelectedDefault = 'M';
  civilSelectedDefault = 'S';

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private _api: ApiRestService
  ) { }
  
  ngOnInit() { 
    this._api.getAllFrom('afiliados').subscribe( res =>
      this.afiliados = res
    );
  }

  onSubmit(familiar: FamiliarI){
    this.updateRecord(familiar);
  }

  updateRecord(form: FamiliarI) {
    this._api.add(form).subscribe(
      response => { 
        if (response["status"] == "error") {
          this.toastr.error('Todos los campos deben estar completos!.');
        }else if(response["status"] == "success"){
          this.toastr.success('Cambios guardados');
          this.router.navigate(['admin/familiares']);
        }     
      },
      // error => {
      //   console.error(error);
      // }
    );

  }

  searchChangeAfiliado(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    this.familiarForm.controls.afiliado.setValue(event.value);
  }

  onBack() {
    this.router.navigate(['admin/familiares']);
  }

}
