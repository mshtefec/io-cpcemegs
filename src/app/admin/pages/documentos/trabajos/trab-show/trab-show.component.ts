import { Component, OnInit }                    from '@angular/core';
import { FormGroup, FormControl, Validators }   from '@angular/forms';
import { Router, ActivatedRoute }               from '@angular/router';
import { ToastrService }                        from 'ngx-toastr';
import { TrabajoService }                       from '../../../../../services/trabajo.service';
import { TrabajoI }                             from '../../../../../interfaces/trabajo.interface';

@Component({
  selector: 'app-trab-show',
  templateUrl: './trab-show.component.html',
  styleUrls: ['./trab-show.component.scss']
})
export class TrabShowComponent implements OnInit {

  trabajoId: number;
  
  traForm = new FormGroup({
    tra_nroasi: new FormControl('', Validators.required),
    tra_nrolegali: new FormControl('', Validators.required),
  });

  constructor(
    private toastr: ToastrService,
    public router: Router,
    private route: ActivatedRoute,
    private _trabajoService: TrabajoService
  ) { }
  
  ngOnInit() { 
    this.route.params.subscribe(
      params => {
        this.trabajoId = +params['id'];

        this._trabajoService.getTrabajo(this.trabajoId).subscribe(
          response => {
            this.traForm.controls.tra_nroasi.setValue(response.tra_nroasi);
            this.traForm.controls.tra_nrolegali.setValue(response.tra_nrolegali);
          },
          error => {
            console.error(error);
          }
        );
      }
    );
  }

  onBack() {
    this.router.navigate(['admin/trabajos']);
  }
}