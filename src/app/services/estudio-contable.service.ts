import { Injectable }         from '@angular/core';
import { HttpClient }         from '@angular/common/http';

import { Observable }         from 'rxjs';
import { map }                from 'rxjs/operators';

import { environment }        from '../../environments/environment';

import { EstudiosContablesI } from '../interfaces/estudios-contables.interface';

@Injectable({
  providedIn: 'root'
})
export class EstudioContableService {
  public url = environment.web_api_url_base + '/api/estudioContable';

  constructor(private http: HttpClient) { }

  getEstudioContable(): Observable<EstudiosContablesI[]> {
      return this.http.get<EstudiosContablesI[]>(this.url).pipe( 
          map( data => {
              return data;
          })
      );
  }

  getOneEstudioContable(id: number): Observable<EstudiosContablesI> {
      return this.http.get<EstudiosContablesI>(this.url + '/' + id);
  }
  
  addEstudioContable(EC: EstudiosContablesI): Observable<EstudiosContablesI> {
      return this.http.post(this.url + '/new', EC);
  }

  updateEstudioContable(EC: EstudiosContablesI): Observable<EstudiosContablesI> {
      return this.http.put(this.url + '/edit/' + EC.id, EC);
  }

  deletedEstudioContable(id: number): Observable<EstudiosContablesI> {
      return this.http.delete(this.url + '/deleted/' + id);
  }
}
