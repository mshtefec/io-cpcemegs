// MODULES
import { NgModule }                           from '@angular/core';
import { CommonModule }                       from '@angular/common';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { IonicModule }                        from '@ionic/angular';
import { IonicSelectableModule }              from 'ionic-selectable';
import { NgxMaskModule }                      from 'ngx-mask';
import { ToastrModule }                       from 'ngx-toastr';
import { DebitosRoutingModule }               from './debitos-routing.module';
import { MaterialModule }                     from './../../../thirdmodules/material.module';

// SERVICES
import { ChequeService }                      from './../../../services/cheque.service';

// COMPONENTS
import { DebitosComponent }                   from './debitos.component';

import { AdhsDebitoListComponent }            from './tarjetas/adhesiones/adhs-list/adhs-debito-list.component';
import { AdhsDebitoNewComponent }             from './tarjetas/adhesiones/adhs-new/adhs-debito-new.component';
import { ExportarDebitoComponent }            from './tarjetas/exportar/exportar-debito.component';
import { RecibosDebitoListComponent }         from './tarjetas/recibos/recibos-list/recibos-debito-list.component';
import { GenerarRecibosDebitoComponent }      from './tarjetas/recibos/recibos-generate/generar-recibos-debito.component';
import { CListComponent }                     from './caja-de-ahorro/c-list/c-list.component';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      
      IonicModule.forRoot(),
      IonicSelectableModule,
      
      NgxMaskModule.forRoot(),
      ToastrModule.forRoot(),
      
      DebitosRoutingModule,

      MaterialModule
    ],
    declarations: [      
      //componentes
      DebitosComponent,
      
      AdhsDebitoListComponent,
      AdhsDebitoNewComponent,
      ExportarDebitoComponent,
      RecibosDebitoListComponent,
      GenerarRecibosDebitoComponent,

      CListComponent
    ],
    providers: [
      ChequeService
    ],
  })
  export class DebitosModule { }